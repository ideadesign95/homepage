<?php
$title = "Đổi mật khẩu";
include_once('./views/header.php');
?>
<style>
    .nar__cover {
        display: none;
    }
</style>
<div class="main__content">
    <div class="content mt-5">
        <!-- title changed-->
        <h3 class="main__tit">Đổi mật khẩu</h3>
        <!-- content start here-->
        <div class="form__content pt-5">
            <form id="form-quen-mat-khau" autocomplete="off">
                <div class="form-group">
                    <input type="hidden" name="code">
                </div>
                <div class="form-group">
                    <label>Mật khẩu mới<span>*</span></label>
                    <input type="password" class="input__style" placeholder="Nhập mật khẩu mới" title="Nhập mật khẩu mới" name="password" data-required>
                    <div class="hint"></div>
                </div>
                <div class="form-group">
                    <label>Xác nhận mật khẩu mới<span>*</span></label>
                    <input type="password" class="input__style" placeholder="Nhập lại mật khẩu mới" title="Nhập lại mật khẩu mới" name="passwordConfirmation" data-required>
                    <div class="hint"></div>
                </div>
                <div class="submit__cover step1-btn">
                    <button class="next" id="button-submit">Đổi mật khẩu</button>
                </div>
            </form>
        </div>
        <!-- content end here-->
    </div>
</div>

<script>
    $(document).ready(() => {
        $('#form-quen-mat-khau input[type="hidden"]').val(getParameterByName('code'));

        $('#button-submit').click((e) => {
            e.preventDefault();

            if (!checkNull('#form-quen-mat-khau'))
                return false;

            if ($('input[name="password"]').val() !== $('input[name="passwordConfirmation"]').val()) {
                $('input[name="passwordConfirmation"]').parents('.form-group').addClass('error');
                $('input[name="passwordConfirmation"]').next('.hint').text("Mật khẩu xác nhận không trùng khớp");
                return false;
            }

            // get form data
            var form = $('#form-quen-mat-khau');
            var form_data = JSON.stringify(form.serializeObject());

            $.ajax({
                url: URL_API + "/auth/reset-password",
                type: "POST",
                contentType: 'application/json',
                data: form_data,
                success: function(result) {
                    if (result.jwt != undefined && result.jwt != '') {
                        // store jwt to cookie
                        setCookie("jwt", result.jwt, 2);
                        setCookie("user", result.user.username, 2);
                        setCookie("result", result.user.id, 2);

                        // redirect to homepage
                        swal({
                            title: "Thông báo",
                            text: "Đổi mật khẩu thành công.",
                            icon: "success"
                        }).then((value) => {
                            window.location.href = "/";
                        });
                    }
                },
                error: function(xhr, textStatus) {
                    var sendData = JSON.stringify({ xhr: xhr, request: form_data });
                    $.ajax({
                        url: URL_API + "/logs",
                        type: "POST",
                        contentType: 'application/json',
                        data: sendData,
                        success: function(result) {},
                        error: function (jXHR, status) {}
                    });
                    swal({
                        title: "Thông báo",
                        text: "Mã xác nhận sai hoặc đã hết hạn, vui lòng sử dụng lại chức năng quên mật khẩu.",
                        icon: "error"
                    }).then((value) => {
                        $('#taikhoan').modal('hide');
                        $('#modal-quenmatkhau').modal('show');
                    });
                    console.log(xhr);
                }
            });

            return false;
        });
    });
</script>

<?php
include_once('./views/footer.php');
?>
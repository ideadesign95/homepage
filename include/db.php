<?php
class Database
{
    public $conn;

    // specify your own database credentials
    private $host = "112.197.2.168";
    private $port = "3306";
    private $db_name = "enerteam";
    private $username = "yan.vps";
    private $password = 'MySQL@#2!';

    // get the database connection
    public function getConnection()
    {
        $this->conn = null;
 
        try
        {
            $this->conn = new PDO("mysql:host=".$this->host.";port=".$this->port.";dbname=".$this->db_name, $this->username, $this->password);
            //$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            //$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception)
        {
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->conn;
    }

    public function closeConnection()
    {
        $this->conn = null;
    }
}

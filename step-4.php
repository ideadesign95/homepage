<?php
$title = "Step 4";
include_once('./views/header.php');
?>

<div class="main__content">
    <?php include_once("./views/top.php"); ?>

    <div class="content mt-5">
        <h3 class="main__tit">Kết Quả Tính Toán & Đánh giá</h3>
        <div class="form__content pt-5 form__ketquaaddstyle">
            <div class="ketqua__cover_n">
                <table class="table">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>MỤC</th>
                            <th>CHỈ SỐ</th>
                            <th>ĐƠN VỊ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Điện (hd)</td>
                            <td name="dien-hd"></td>
                            <td>kWh</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Điện (khác)</td>
                            <td name="dien-khac"></td>
                            <td>kWh</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Điện (gián tiếp)</td>
                            <td name="dien-gian-tiep"></td>
                            <td>kWh</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Điện (cb)</td>
                            <td name="dien-cb"></td>
                            <td>kWh</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Tổng sản Lượng quy đổi</td>
                            <td name="tong-san-luong-quy-doi"></td>
                            <td>kg</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Định mức tiêu hao năng lượng</td>
                            <td name="dinh-muc-tieu-hao-nang-luong"></td>
                            <td>kWh/Tấn</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Suất tiêu hao năng lượng</td>
                            <td name="suat-tieu-hao-nang-luong"></td>
                            <td>kWh/Tấn</td>
                        </tr>
                        <tr>
                            <td colspan='4'>
                                <span class="result_label">Đánh giá kết quả:</span>
                                <span class="result_txt">ĐẠT</span>
                                <p class="result_subtxt note-kqua" style="display: none;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- <div class="dothi__coover">
            <h4 class="dothi__tit text-capitalize font-weight-bold text-center pt-5 pb-5">đồ thị so sánh suất tiêu hao của người dùng so với định mức và cơ sở dữ liệu các người dùng khác</h4>
            <div class="dothi_cover-content" style="width: 100%; overflow-x: auto;overflow-y:hidden">
                <canvas id="myChart" width="680" height="230"></canvas>
            </div>
            <h4 class="chart__tit text-center pt-4">
                So sánh SEC
            </h4>
        </div> -->
    </div>

    <!--btn-->
    <div class="submit__cover text-center final__result">
        <button class="back">QUAY LẠI</button>
        <button class="next" name="exportSheet">XUẤT FILE BÁO CÁO (SHEET FORM)</button>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        activeMenu('/index');
        activeStep(4);

        var slqd = {
            'slqdCa': 0,
            'slqdTom': 0
        };
        var step1 = getJson(STEP1_KEY);
        var step2 = getJson(STEP2_KEY);
        var step3 = getJson(STEP3_KEY);

        if (!isObject(step1)) {
            swal({
                title: "Thông báo",
                text: "Vui lòng nhập đầy đủ thông tin ở Bước 1",
                icon: "error"
            }).then((value) => {
                window.location.href = "./index" + window.location.search;
            });
        } else if (!isObject(step2)) {
            swal({
                title: "Thông báo",
                text: "Vui lòng nhập đầy đủ thông tin ở Bước 2",
                icon: "error"
            }).then((value) => {
                window.location.href = "./step-2" + window.location.search;
            });
        } else if (!isObject(step3)) {
            swal({
                title: "Thông báo",
                text: "Vui lòng nhập đầy đủ thông tin ở Bước 3",
                icon: "error"
            }).then((value) => {
                window.location.href = "./step-3" + window.location.search;
            });
        }

        calculate(slqd, step1, step2, step3);
        // drawChart();

        $('button[name="exportSheet"]').click((e) => {
            e.preventDefault();
            var jwt = getCookie('jwt');
            var formId = Number(getParameterByName('form'));
            // if (jwt == undefined || jwt == '') {
            //     swal({
            //         title: "Thông báo",
            //         text: "Vui lòng đăng nhập để sử dụng chức năng này.",
            //         icon: "warning"
            //     }).then((value) => {
            //         // login
            //         $('.modal__dangnhap#taikhoan').modal('show');
            //         $('#dangnhap-tab').tab('show');
            //     });
            //     return false;
            // }

            var jsonData = {
                'step1': step1,
                'step2': step2,
                'step3': step3,
                'step4': {
                    'dien-hd': $('td[name="dien-hd"]').text().replace(/,/g,''),
                    'dien-khac': $('td[name="dien-khac"]').text().replace(/,/g,''),
                    'dien-gian-tiep': $('td[name="dien-gian-tiep"]').text().replace(/,/g,''),
                    'dien-cb': $('td[name="dien-cb"]').text().replace(/,/g,''),
                    'tong-san-luong-quy-doi': $('td[name="tong-san-luong-quy-doi"]').text().replace(/,/g,''),
                    'dinh-muc-tieu-hao-nang-luong': $('td[name="dinh-muc-tieu-hao-nang-luong"]').text().replace(/,/g,''),
                    'suat-tieu-hao-nang-luong': $('td[name="suat-tieu-hao-nang-luong"]').text().replace(/,/g,''),
                },
                'slqd': slqd,
                'form': formId,
                'uid': getCookie('result')
            };

            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + jwt
                },
                url: "/api/export",
                type: "POST",
                data: JSON.stringify(jsonData),
                contentType: 'application/json',
                success: function(response) {
                    console.log(response);
                    if (response.result == false && response.status_code == 403) {
                        swal({
                            title: "Thông báo",
                            text: "Vui lòng đăng nhập để sử dụng chức năng này.",
                            icon: "warning"
                        }).then((value) => {
                            // login
                            $('.modal__dangnhap#taikhoan').modal('show');
                            $('#dangnhap-tab').tab('show');
                        });
                        return false;
                    } else if (response.result == false) {
                        swal({
                            title: "Thông báo",
                            text: response.message,
                            icon: "error"
                        });
                        return false;
                    } else if (response.result == true) {
                        // write
                        var isUpdate = formId != 0;
                        writeForm(isUpdate, formId, jsonData);
                        swal({
                            title: "Thông báo",
                            text: response.message,
                            icon: "success"
                        }).then((value) => {
                            removeLocalStorage('step1');
                            removeLocalStorage('step2');
                            removeLocalStorage('step3');
                            window.open(
                                window.location.protocol + '//' + window.location.hostname + response.url,
                                '_blank' // <- This is what makes it open in a new window.
                            );
                        });
                        return false;
                    }
                },
                error: function(jqXHR, error, errorThrown) {
                    swal({
                        title: "Thông báo",
                        text: "Có lỗi xảy ra",
                        icon: "error"
                    });
                    console.log(jqXHR);
                },
            });
        });

        $('button.back').click((e) => {
            e.preventDefault();
            window.location.href = './step-3' + window.location.search;
        });
    });

    function calculate(objSlqd, step1, step2, step3) {
        // D1
        $('td[name="dien-hd"]').text(numberWithCommas(step2["tong-dien-nang-tieu-thu"]));

        // D2
        var a = step2["check-dien-tieu-thu"] == 'true' ? parseFloat(step2["dien-tieu-thu"]) : parseFloat(step2['dien-tieu-thu-uoc-tinh']);
        var b = parseFloat(step2['khoi-luong-da-ban-ra']) * 70;
        var c = parseFloat(step2['san-luong-cho-thue-kho-lanh']) * parseFloat(step2['thoi-gian-cho-thue-kho-lanh']) * 2.5;
        a = isNaN(a) ? 0 : a.round(2);
        b = isNaN(b) ? 0 : b.round(2);
        c = isNaN(c) ? 0 : c.round(2);
        var sum = a + b + c;
        $('td[name="dien-khac"]').text(isNaN(sum) ? 0 : numberWithCommas(sum));

        // D3
        a = parseFloat(step2['khoi-luong-da-mua-vao']) * 70;
        b = parseFloat(step2['san-luong-thue-kho-lanh']) * parseFloat(step2['thoi-gian-thue-kho-lanh']) * 2.5;
        a = isNaN(a) ? 0 : a.round(2);
        b = isNaN(b) ? 0 : b.round(2);
        sum = a + b;
        $('td[name="dien-gian-tiep"]').text(isNaN(sum) ? 0 : numberWithCommas(sum));

        // D4
        a = parseFloat($('td[name="dien-hd"]').text().replace(/,/g,''));
        b = parseFloat($('td[name="dien-khac"]').text().replace(/,/g,''));
        c = parseFloat($('td[name="dien-gian-tiep"]').text().replace(/,/g,''));
        a = isNaN(a) ? 0 : a.round(2);
        b = isNaN(b) ? 0 : b.round(2);
        c = isNaN(c) ? 0 : c.round(2);
        sum = a - b + c;
        $('td[name="dien-cb"]').text(isNaN(sum) ? 0 : numberWithCommas(sum));

        // D5
        var slqdCa = 0;
        var slqdTom = 0;
        var temp = 0;
        for (const key in step3) {
            if (step3[key] != '' || step3[key] != 0) {
                if (key.includes("loai-san-pham-ca-da-tron")) {
                    temp = isNaN(step3[key]) ? 1 : parseFloat(step3[key]);
                }
                if (key.includes("san-luong-ca")) {
                    temp = parseFloat(temp) * parseFloat(step3[key]);
                    slqdCa += temp.round(2);
                }
                if (key.includes("loai-san-pham-tom")) {
                    temp = isNaN(step3[key]) ? 1 : parseFloat(step3[key]);
                }
                if (key.includes("san-luong-tom")) {
                    temp = parseFloat(temp) * parseFloat(step3[key]);
                    slqdTom += temp.round(2);
                }
            }
        }
        objSlqd.slqdCa = slqdCa;
        objSlqd.slqdTom = slqdTom;
        if (slqdCa >= slqdTom) {
            $('td[name="tong-san-luong-quy-doi"]').prev('td').text('Tổng sản lượng quy đổi sang cá da trơn');
            $('td[name="suat-tieu-hao-nang-luong"]').prev('td').text('Suất tiêu hao năng lượng quy đổi sang sản phẩm cá da trơn');
            $('td[name="tong-san-luong-quy-doi"]').text(numberWithCommas(Number((slqdCa * 1) + (slqdTom * 1.78)).round(2)));
        } else {
            $('td[name="tong-san-luong-quy-doi"]').prev('td').text('Tổng sản lượng quy đổi sang tôm');
            $('td[name="suat-tieu-hao-nang-luong"]').prev('td').text('Suất tiêu hao năng lượng quy đổi sang sản phẩm tôm');
            $('td[name="tong-san-luong-quy-doi"]').text(numberWithCommas(Number((slqdCa * 0.56) + (slqdTom * 1)).round(2)));
        }

        // D6
        if (!isTodayGreater(1, 1, 2026)) {
            if (slqdCa >= slqdTom) {
                $('td[name="dinh-muc-tieu-hao-nang-luong"]').prev('td').text('Định mức tiêu hao năng lượng cho sản phẩm cá da trơn');
                $('td[name="dinh-muc-tieu-hao-nang-luong"]').text(numberWithCommas(1050));
            } else {
                $('td[name="dinh-muc-tieu-hao-nang-luong"]').prev('td').text('Định mức tiêu hao năng lượng cho sản phẩm tôm');
                $('td[name="dinh-muc-tieu-hao-nang-luong"]').text(numberWithCommas(2050));
            }
        } else {
            if (slqdCa >= slqdTom) {
                $('td[name="dinh-muc-tieu-hao-nang-luong"]').prev('td').text('Định mức tiêu hao năng lượng cho sản phẩm cá da trơn');
                $('td[name="dinh-muc-tieu-hao-nang-luong"]').text(numberWithCommas(900));
            } else {
                $('td[name="dinh-muc-tieu-hao-nang-luong"]').prev('td').text('Định mức tiêu hao năng lượng cho sản phẩm tôm');
                $('td[name="dinh-muc-tieu-hao-nang-luong"]').text(numberWithCommas(1625));
            }
        }

        // D7
        var d4 = parseFloat($('td[name="dien-cb"]').text().replace(/,/g,'')).round(2);
        var d5 = parseFloat($('td[name="tong-san-luong-quy-doi"]').text().replace(/,/g,'')).round(2);
        var d7 = Number(d4 / d5 * 1000).round(2);
        if (d7 == 0) {
            for (let i = 3; i < 10; i++) {
                d7 = Number(d4 / d5 * 1000).round(i);
                if (d7 != 0)
                    break;
            }
        }
        var text = 'Suất tiêu hao năng lượng quy đổi sang sản phẩm cá da trơn';
        if (slqdCa < slqdTom)
            text = 'Suất tiêu hao năng lượng quy đổi sang sản phẩm tôm';
        $('td[name="suat-tieu-hao-nang-luong"]').prev('td').text(text);
        $('td[name="suat-tieu-hao-nang-luong"]').text(numberWithCommas(d7));

        // D8
        var d6 = parseFloat($('td[name="dinh-muc-tieu-hao-nang-luong"]').text().replace(/,/g,''));
        // d7 = parseFloat($('td[name="suat-tieu-hao-nang-luong"]').text().replace(/,/g,''));
        if (d7 > d6) {
            $('.note-kqua').show();
            $('.note-kqua').text('Suất tiêu hao năng lượng của cơ sở không đáp ứng định mức tiêu hao năng lượng cho quá trình chế biến công nghiệp của các nhóm sản phẩm cá da trơn và tôm');
            $('.result_txt').text("KHÔNG ĐẠT");
            $('.result_txt').addClass('bg-danger');
        } else {
            $('.note-kqua').show();
            $('.note-kqua').text('Suất tiêu hao năng lượng của cơ sở phù hợp với quy định định mức tiêu hao năng lượng cho quá trình chế biến công nghiệp của các nhóm sản phẩm cá da trơn và tôm');
            $('.result_txt').text("ĐẠT");
            $('.result_txt').removeClass('bg-danger');
        }
    }

    function drawChart() {
        var ctx = document.getElementById('myChart');
        var secTrungBinh = 1080;
        // var secNhaMay = toFixedNumber($('td[name="suat-tieu-hao-nang-luong"]').text().replace(/,/g,'')).round(2);
        var secNhaMay = parseFloat($('td[name="suat-tieu-hao-nang-luong"]').text().replace(/,/g,'')).round(2);
        var secDinhMuc = parseInt($('td[name="dinh-muc-tieu-hao-nang-luong"]').text().replace(/,/g,''));
        var max = Math.max(secTrungBinh, secNhaMay, secDinhMuc) + 100;
        var step = Math.ceil(max / 10 / 5) * 5;

        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: ['SEC trung bình', 'SEC nhà máy', 'SEC định mức'],
                datasets: [{
                    label: 'So sánh SEC',
                    data: [secTrungBinh, secNhaMay, secDinhMuc],
                    backgroundColor: [
                        'rgba(150,217,238,1)',
                        'rgba(44,180,221,1)',
                        'rgba(29,141,176,1)'

                    ],
                }]
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false,
                },
                animation: {
                    "onComplete": function() {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.fillStyle = "rgba(0, 0, 0, 1)";
                        ctx.textBaseline = 'bottom';
                        ctx.fontStyle = 'normal';
                        ctx.fontSize = '18';
                        // Loop through each data in the datasets
                        this.data.datasets.forEach(function(dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function(bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x + 30, bar._model.y + 4);

                            });
                        });
                    }
                },
                scales: {
                    yAxes: [{
                        barPercentage: 0.7,
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            fontColor: "#026D8E",
                            fontSize: 18,
                            fontStyle: "500",

                        },

                    }],
                    xAxes: [{
                        gridLines: {
                            display: true,
                            color: "#E8EBE9",
                            lineWidth: 1,
                            zeroLineColor: '#FFFFFF',

                        },
                        ticks: {
                            fontColor: "#001408",
                            fontSize: 18,
                            padding: 5,
                            fontStyle: "normal",
                            min: 000,
                            max: max,
                            stepSize: step,
                        },
                    }]
                },
                legend: {
                    display: false
                },
            }
        });
    }

    function writeForm(isUpdate = false, formId = 0, jsonData) {
        jwt = getCookie('jwt');
        if (jwt != '') {
            var method = isUpdate ? "PUT" : "POST";
            var url = URL_API + (isUpdate ? `/forms/${formId}` : "/forms");
            var uid = getCookie('result');
            const formData = new FormData();
            const data = {};
            data['user_id'] = uid;
            data['created_by'] = uid;
            data['updated_by'] = uid;
            data['data'] = jsonData;
            formData.append('data', JSON.stringify(data));
            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + jwt
                },
                url: url,
                type: method,
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {},
                error: () => {}
            });
        }
    }
</script>

<?php
include_once('./views/footer.php');
?>
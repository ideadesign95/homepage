<?php
$title = "Giải pháp";
include_once('./views/header.php');
?>

<div class="main__content">
    <div class="content mt-5">
        <!-- title changed-->
        <h3 class="main__tit">MỘT SỐ GIẢI PHÁP NÂNG CAO HIỆU QUẢ SỬ DỤNG NĂNG LƯỢNG TRONG NGÀNH CHẾ BIẾN THỦY SẢN</h3>
        <!-- content start here-->
        <div class="pt-5 pb-5 giaiphap__content container">
            <div class="txt">
                <p class="font-weight-bold">1. Nhóm giải pháp hoàn thiện quy trình sản xuất, quản lý sử dụng năng lượng</p>
                <p>
                    a) Hạn chế các băng chuyền IQF chạy không tải, non tải. Trong trường hợp phải chạy non tải cần có giải pháp điều chỉnh
                    năng suất hệ thống lạnh;</br>
                    b) Sử dụng hợp lý các thiết bị cấp đông;</br>
                    c) Quản lý chặt chẽ việc sản xuất và sử dụng nước đá;</br>
                    d) Quản lý việc sử dụng kho lạnh, đóng cửa kho khi không có người hay xe đi qua;</br>
                    đ) Duy trì nhiệt độ kho lạnh ở mức cần thiết;</br>
                    e) Quản lý chặt chẽ việc sử dụng hệ thống điều hòa nhiệt độ;</br>
                    g) Thay đổi cách thức sử dụng thiết bị phù hợp khi công suất chế biến thấp;</br>
                    h) Các giải pháp khác.
                </p>
            </div>
            <div class="txt">
                <p class="font-weight-bold">2. Nhóm giải pháp hoàn thiện qui trình vận hành thiết bị lạnh</p>
                <p>
                    a) Không để các máy nén chạy non tải;</br>
                    b) Không vận hành kho lạnh, đá vẩy với thiết bị cấp đông trên cùng 1 nhiệt độ sôi;</br>
                    c) Duy trì áp suất hút ở mức hợp lý;</br>
                    d) Xả tuyết kho lạnh đúng quy trình, không để tuyết bám nhiều trên dàn lạnh.</br>
                </p>
            </div>

            <div class="txt">
                <p class="font-weight-bold">3. Nhóm giải pháp sửa chữa, bảo dưỡng thiết bị lạnh</p>
                <p>
                    a) Kiểm tra, khắc phục việc suy giảm công suất của các băng chuyền IQF;</br>
                    b) Xác định công suất lạnh máy nén trục vít đã chạy lâu năm;</br>
                    c) Lập hồ sơ theo dõi cho từng máy nén trục vít;</br>
                    d) Kiểm tra, sửa chữa kịp thời các thiết bị hỏng hóc hoặc bị xuống cấp;</br>
                    đ) Thường xuyên kiểm tra, xử lý hiện tượng bám cáu cặn trên dàn ngưng.</br>
                </p>
            </div>

            <div class="txt">
                <p class="font-weight-bold">3. Nhóm giải pháp sửa chữa, bảo dưỡng thiết bị lạnh</p>
                <p>
                    a) Kiểm tra, khắc phục việc suy giảm công suất của các băng chuyền IQF;</br>
                    b) Xác định công suất lạnh máy nén trục vít đã chạy lâu năm;</br>
                    c) Lập hồ sơ theo dõi cho từng máy nén trục vít;</br>
                    d) Kiểm tra, sửa chữa kịp thời các thiết bị hỏng hóc hoặc bị xuống cấp;</br>
                    đ) Thường xuyên kiểm tra, xử lý hiện tượng bám cáu cặn trên dàn ngưng.</br>
                </p>
            </div>

            <div class="txt">
                <p class="font-weight-bold">4. Nhóm giải pháp thiết kế, lắp đặt lại hệ thống lạnh</p>
                <p>
                    a) Thiết kế hệ thống lạnh trung tâm sử dụng môi chất NH3 thay thế các thiết bị đơn lẻ;</br>
                    b) Thiết kế lại hệ thống lạnh để đảm bảo máy nén không vận hành non tải và ở nhiệt độ sôi thấp;</br>
                    c) Qui hoạch lại các kho lạnh, hệ thống điều hòa nhiệt độ;</br>
                    d) Cấp nước lạnh vào máy chế biến đá vẩy;</br>
                    đ) Cấp NH3 lỏng từ bình hạ áp kho lạnh vào bình hạ áp của băng chuyền IQF;</br>
                    e) Chuyển đổi một số thiết bị lạnh sử dụng môi chất R22 sang NH3;</br>
                    g) Thay máy nén có hiệu suất phát lạnh thấp bằng máy nén có hiệu suất phát lạnh cao hơn;</br>
                    h) Lắp đặt hầm đông lạnh để cấp đông cá nguyên con;</br>
                    i) Kiểm soát áp suất ngưng trôi nổi</br>
                    k) Lắp biến tần cho máy nén trong trường hợp cần thiết;</br>
                    l) Các giải pháp thiết kế, lắp đặt, đầu tư khác.</br>
                </p>
            </div>

            <div class="txt">
                <p class="font-weight-bold">5. Nhóm giải pháp sử dụng thiết bị ngoại vi nâng hiệu suất phát lạnh</p>
                <p>
                    a) Lắp thêm áp kế chân không cho các hệ thống cấp đông để theo dõi áp suất hút;</br>
                    b) Lắp cảm biến CO2 và quạt thu hồi nhiệt cho các phòng chế biến để tăng hiệu suất phát lạnh và giảm lượng điện tiêu</br>
                    thụ cho hệ thống điều hòa nhiệt độ;</br>
                    c) Lắp thêm van điện từ xả khí không ngưng lần lượt từng dàn ngưng nhằm giảm áp suất ngưng, giảm lượng điện tiêu thụ
                    của máy nén;</br>
                    d) Lắp thêm thiết bị tách khí, tách nước cho hệ thống lạnh.</br>
                </p>
            </div>
        </div>
        <!-- content end here-->
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        activeMenu('/giai-phap');
    });
</script>

<?php
include_once('./views/footer.php');
?>
#Get base image
FROM ubuntu:16.04

#Install package
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update && apt-get -y upgrade

# Use the default UTF-8 language.
ENV LANG C.UTF-8

RUN apt-get -y update --fix-missing
RUN apt-get update && apt-get -y install software-properties-common && add-apt-repository ppa:ondrej/php && apt-get update

RUN apt-get install -y apache2
RUN apt-get install -y curl

RUN apt-get update && apt-get install -y --no-install-recommends \
	libapache2-mod-php7.2 \
	php7.2 \
	php7.2-bcmath \
	php7.2-cli \
	php7.2-curl \
	php7.2-dev \
	php7.2-gd \
	php7.2-imap \
	php7.2-intl \
	php7.2-mbstring \
	php7.2-mysql \
	php7.2-pgsql \
	php7.2-pspell \
	php7.2-xml \
	php7.2-xmlrpc \
	php7.2-zip \
	php7.2-soap \
	php-apcu \
	php-memcached \
	php-pear \
	php-redis \
	&& apt-get clean \
	&& rm -fr /var/lib/apt/lists/*

RUN rm -f /etc/apache2/sites-enabled/000-default.conf
RUN rm -f /etc/php/7.2/apache2/php.ini
RUN a2enmod headers
RUN a2enmod rewrite
RUN a2dismod mpm_event
RUN a2enmod mpm_prefork
RUN a2enmod proxy_fcgi setenvif
RUN phpenmod pdo

#install composer
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
#RUN composer --version

#Working directory
#WORKDIR /home/ftp168/enerteam

#start apache2
ADD ./.docker/home.conf /etc/apache2/sites-enabled
EXPOSE 80
CMD apachectl -D FOREGROUND
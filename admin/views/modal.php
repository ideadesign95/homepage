<!-- modal change pass -->
<div class="modal" id="modal-change-password">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Đổi mật khẩu</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form-quen-mat-khau" autocomplete="off">
                    <div class="form-group">
                        <input type="hidden" name="uid">
                    </div>
                    <div class="form-group">
                        <label for="password">Mật khẩu mới <span style="color: red;">*</span></label>
                        <input type="password" class="form-control" placeholder="Nhập mật khẩu mới" title="Nhập mật khẩu mới" name="password" autocomplete="off">
                        <div class="hint" style="color: red; font-size: 12px;"></div>
                    </div>
                    <div class="form-group">
                        <label for="passwordConfirmation">Xác nhận mật khẩu mới <span style="color: red;">*</span></label>
                        <input type="password" class="form-control" placeholder="Nhập lại mật khẩu mới" title="Nhập lại mật khẩu mới" name="passwordConfirmation" autocomplete="off">
                        <div class="hint" style="color: red; font-size: 12px;"></div>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-success" id="button-submit">Đổi mật khẩu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(() => {
        $('#modal-change-password input[name="uid"]').val(getCookie('result'));
        $('a[data-toggle="modal"]').click((e) => {
            e.preventDefault();
            var selector = $(e.target).attr('data-target');
            $(selector).modal('show');
        });

        $('#button-submit').click((e) => {
            e.preventDefault();

            if ($('#modal-change-password input[name="password"]').val() == '') {
                $('#modal-change-password input[name="password"]').next('.hint').text("Vui lòng điền đầy đủ thông tin.");
                return false;
            }
            if ($('#modal-change-password input[name="passwordConfirmation"]').val() == '') {
                $('#modal-change-password input[name="passwordConfirmation"]').next('.hint').text("Vui lòng điền đầy đủ thông tin.");
                return false;
            }

            if ($('#modal-change-password input[name="password"]').val() !== $('#modal-change-password input[name="passwordConfirmation"]').val()) {
                $('#modal-change-password input[name="passwordConfirmation"]').parents('.form-group').addClass('error');
                $('#modal-change-password input[name="passwordConfirmation"]').next('.hint').text("Mật khẩu xác nhận không trùng khớp");
                return false;
            }

            // get form data
            const formData = new FormData();
            formData.append('password', $('#modal-change-password input[name="password"]').val());

            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + getCookie('jwt')
                },
                url: URL_API + '/users/' + $('#modal-change-password input[name="uid"]').val(),
                data: formData,
                processData: false,
                contentType: false,
                type: 'PUT',
                success: function(result) {
                    if (result != undefined && result != '') {
                        // redirect to homepage
                        swal({
                            title: "Thông báo",
                            text: "Đổi mật khẩu thành công.",
                            icon: "success"
                        }).then((value) => {
                            $('#modal-change-password input[name="password"]').val('');
                            $('#modal-change-password input[name="passwordConfirmation"]').val('');
                            $('#modal-change-password').modal('hide');
                        });
                    }
                },
                error: function(xhr, textStatus) {
                    swal({
                        title: "Thông báo",
                        text: "Có lỗi xảy ra trong quá trình đổi mật khẩu",
                        icon: "error",
                        timer: 2000
                    });
                    console.log(xhr);
                }
            });

            return false;
        });
    });
</script>
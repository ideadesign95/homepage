<?php
include_once("./include/const.php");
include_once("./include/functions.php");
if (!isset($_COOKIE['jwt']) || !isset($_COOKIE['user']) || !isset($_COOKIE['permissions']))
    header("Location: /admin/login");

$permissions = array_map('trim', explode(',', $_COOKIE['permissions']));
if (!is_array($permissions)) {
    setcookie("token", "", time() - 3600);
    setcookie("user", "", time() - 3600);
    setcookie("permissions", "", time() - 3600);
    header("Location: /admin/login");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Enerteam">
    <meta name="author" content="Enerteam">
    <meta name="keywords" content="Enerteam">

    <!-- Title Page-->
    <title><?php echo !isset($title) ? 'Dashboard' : $title; ?></title>

    <!-- Fontfaces CSS-->
    <link href="./css/font-face.css" rel="stylesheet" media="all">
    <link href="./vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="./vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="./vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="./vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="./vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="./vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="./vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="./vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="./vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="./vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="./vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Zoom -->
    <link href="./css/zoom.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="./css/theme.css" rel="stylesheet" media="all">
    <link rel="shortcut icon" href="http://enerteam.org/wp-content/themes/fokatech/favicon.png">

    <!-- Markdown -->
    <link href="./css/bootstrap-markdown.min.css" rel="stylesheet" media="all">

    <!-- Jquery JS-->
    <script src="./vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="./vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="./vendor/bootstrap-4.1/bootstrap.min.js"></script>
</head>

<body>
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <!-- <a class="logo" href="index.html">
                            <img src="images/icon/logo.png" alt="CoolAdmin" />
                        </a> -->
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a href="/admin/index">
                                <i class="fas fa-list-ul"></i>Dashboard</a>
                        </li>
                        <?php foreach ($permissions as $value) {
                            if (in_array($value, PERMISSIONS_ADMIN)) {
                                $li = "";
                                switch ($value) {
                                    case 'manageUser':
                                        $li = '<li><a href="/admin/users"><i class="fas fa-users"></i>Quản lý người dùng</a></li>';
                                        break;
                                    case 'manageForm':
                                        $li = '<li><a href="/admin/forms"><i class="fas fa-file-text"></i>Quản lý báo cáo</a></li>';
                                        break;
                                    case 'referenceGraph':
                                        $li = '<li><a href="/admin/forms"><i class="fas fa-bar-chart-o"></i>Đồ thị tham khảo</a></li>';
                                        break;
                                    default:
                                        # code...
                                        break;
                                }
                                echo $li;
                            }
                        } ?>
                        <li>
                            <a href="/">
                                <i class="fas fa-arrow-left"></i>Quay về trang chủ</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo justify-content-center">
                <a href="/admin">
                    <img src="images/icon/logo-white.png" alt="Enerteam" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a href="/admin/index">
                                <i class="fas fa-list-ul"></i>Dashboard</a>
                        </li>
                        <?php foreach ($permissions as $value) {
                            if (in_array($value, PERMISSIONS_ADMIN)) {
                                $li = "";
                                switch ($value) {
                                    case 'manageUser':
                                        $li = '<li><a href="/admin/users"><i class="fas fa-users"></i>Quản lý người dùng</a></li>';
                                        break;
                                    case 'manageForm':
                                        $li = '<li><a href="/admin/forms"><i class="fas fa-file-text"></i>Quản lý báo cáo</a></li>';
                                        break;
                                    case 'referenceGraph':
                                        $li = '<li><a href="/admin/reference"><i class="fas fa-bar-chart-o"></i>Đồ thị tham khảo</a></li>';
                                        break;
                                    default:
                                        # code...
                                        break;
                                }
                                echo $li;
                            }
                        } ?>
                        <li>
                            <a href="/">
                                <i class="fas fa-arrow-left"></i>Quay về trang chủ</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap justify-content-between">
                            <h5 class="font-green" style="width: 80%;">TÍNH TOÁN ĐỊNH MỨC TIÊU HAO NĂNG LƯỢNG TRONG NGÀNH CÔNG NGHIỆP CHẾ BIẾN THỦY SẢN ÁP DỤNG CHO QUÁ TRÌNH CHẾ BIẾN CÔNG NGHIỆP CỦA CÁC NHÓM SẢN PHẨM CÁ DA TRƠN VÀ TÔM THEO THÔNG TƯ 52/2018/TT-BCT</h5>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $_COOKIE['user'] ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="account-dropdown__item">
                                                <a href="/admin/index">
                                                    <i class="zmdi zmdi-settings"></i>Dashboard</a>
                                            </div>
                                            <div class="account-dropdown__item">
                                                <a href="#" data-toggle="modal" data-target="#modal-change-password">
                                                    <i class="zmdi zmdi-account-circle"></i>Đổi mật khẩu</a>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="#" id="logout">
                                                    <i class="zmdi zmdi-power"></i>Đăng xuất</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->
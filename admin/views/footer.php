    </div>

    <!-- Vendor JS -->
    <script src="./vendor/slick/slick.min.js"></script>
    <script src="./vendor/wow/wow.min.js"></script>
    <script src="./vendor/animsition/animsition.min.js"></script>
    <script src="./vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="./vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="./vendor/counter-up/jquery.counterup.min.js"></script>
    <script src="./vendor/circle-progress/circle-progress.min.js"></script>
    <script src="./vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="./vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="./vendor/select2/select2.min.js"></script>
    <script src="./js/zoom.js"></script>
    <script src="./js/bootstrap-markdown.js"></script>
    <script src="./js/markdown.js"></script>
    <script src="./js/to-markdown.js"></script>
    <script src="./js/dropzone.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Main JS-->
    <?php include_once('const.js.php') ?>
    <script src="./js/main.js"></script>
    <script src="./js/script.js"></script>

    <script>
        $('#logout').click(function() {
            logout();
        });
    </script>

    <?php include_once('modal.php') ?>

    </body>

    </html>
    <!-- end document-->
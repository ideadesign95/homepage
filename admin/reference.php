<?php
$title = "Quản lý đồ thị tham khảo";
include './views/header.php';
?>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid" id="listData">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Quản lý đồ thị tham khảo</h2>
                        <button class="au-btn au-btn-icon au-btn--green" rel="addData">
                            <i class="zmdi zmdi-plus"></i>Thêm đồ thị tham khảo</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive table--no-card m-b-30">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên đồ thị</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <ul class="pagination float-right">
                        <li class="page-item disabled" id="pager-prev"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                        <li class="page-item" id="pager-next"><a class="page-link" href="#"><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="addData" style="display: none;">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Thêm đồ thị tham khảo</h2>
                        <div class="button-wrap">
                            <button class="au-btn au-btn-icon btn-danger" rel="back">
                                <i class="zmdi zmdi-close"></i>Hủy
                            </button>
                            <button class="au-btn au-btn-icon au-btn--green" rel="saveNewData">
                                <i class="zmdi zmdi-check"></i>Lưu
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal" autocomplete="off">
                                <div class="row form-group">
                                    <div class="col-12">
                                        <label for="title" class=" form-control-label d-block">Tên đồ thị tham khảo</label>
                                        <input type="text" name="title" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row form-group mt-5">
                                    <div class="col-12">
                                        <button type="button" class="btn btn-outline-success m-t-25 d-block m-auto" rel="addMore">Thêm danh mục</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="editData" style="display: none;">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Sửa đồ thị tham khảo</h2>
                        <div class="button-wrap">
                            <button class="au-btn au-btn-icon btn-danger" rel="back">
                                <i class="zmdi zmdi-close"></i>Hủy
                            </button>
                            <button class="au-btn au-btn-icon au-btn--green" rel="saveData">
                                <i class="zmdi zmdi-check"></i>Lưu
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal" autocomplete="off">
                                <div class="row form-group">
                                    <input type="hidden" id="id">
                                    <div class="col-12">
                                        <label for="title" class=" form-control-label d-block">Tên đồ thị tham khảo</label>
                                        <input type="text" name="title" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row form-group mt-5">
                                    <div class="col-12">
                                        <button type="button" class="btn btn-outline-success m-t-25 d-block m-auto" rel="addMore">Thêm danh mục</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->
</div>
<script>
    var index = 1;
    var start = 0;
    $(document).ready(function() {
        activeMenu('/admin/reference');
        getList('/reference-graphs?_sort=id:DESC&_start=' + start + '&_limit=10', 'table tbody', appendTable);
        $('#listData').show();
        $('#addData').hide();
        $('#editData').hide();

        $('button[rel="addData"]').click(function(e) {
            e.preventDefault();
            $('#listData').hide();
            $('#editData').hide();
            $('#addData').show(300);
        });

        $('button[rel="back"]').click(function(e) {
            e.preventDefault();
            $('input[name="title"]').val('');
            $('input[name="label"]').val('');
            $('input[name="value"]').val('');
            $('.chart-row').remove();
            $('#addData').hide();
            $('#editData').hide();
            $('#listData').show(300);
            getList('/reference-graphs?_sort=id:DESC&_start=' + start + '&_limit=10', 'table tbody', appendTable);
            // $('#pager-prev').addClass('disabled');
            // start = 0;
            index = 1;
        });

        $('button[rel="saveNewData"]').click(function(e) {
            e.preventDefault();
            var sum = 0;
            const formData = new FormData();
            const data = {};
            data['title'] = $('#addData input[name="title"]').val();
            data['data'] = [];
            $.each($('#addData .chart-row'), function(key, row) {
                var label = $(this).find('input[name="label"]').val();
                var value = $(this).find('input[name="value"]').val();
                if (label != '' && value != '') {
                    sum += Number(value);
                    var object = {
                        'label': label,
                        'value': value,
                    };
                    data['data'].push(object);
                }
            });

            if (sum > 100) {
                showAlert('#listData > .row.m-b-25 > .col-md-12', 'danger', 'Tổng giá trị không được lớn hơn 100%');
                $('button[rel="back"]').trigger('click');
                return false;
            }

            formData.append('data', JSON.stringify(data));

            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + getCookie('jwt')
                },
                url: URL_API + '/reference-graphs',
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data) {
                    console.log(data);
                    showAlert('#listData > .row.m-b-25 > .col-md-12', 'success', 'Thêm đồ thị thành công.');
                    $('button[rel="back"]').trigger('click');
                },
                error: function() {
                    // $('button[rel="back"]').trigger('click');
                    showAlert('#addData > div.row.m-b-25 > div', 'danger', 'Có lỗi xảy ra.');
                }
            });
        });

        $('button[rel="saveData"]').click(function(e) {
            e.preventDefault();
            var sum = 0;
            const formData = new FormData();
            const data = {};
            data['title'] = $('#editData input[name="title"]').val();
            data['data'] = [];
            $.each($('#editData .chart-row'), function(key, row) {
                var label = $(this).find('input[name="label"]').val();
                var value = $(this).find('input[name="value"]').val();
                if (label != '' && value != '') {
                    sum += Number(value);
                    var object = {
                        'label': label,
                        'value': value,
                    };
                    data['data'].push(object);
                }
            });

            if (sum > 100) {
                showAlert('#listData > .row.m-b-25 > .col-md-12', 'danger', 'Tổng giá trị không được lớn hơn 100%');
                $('button[rel="back"]').trigger('click');
                return false;
            }

            formData.append('data', JSON.stringify(data));

            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + getCookie('jwt')
                },
                url: URL_API + '/reference-graphs/' + $('#editData input[type=hidden]#id').val(),
                data: formData,
                processData: false,
                contentType: false,
                type: 'PUT',
                success: function(data) {
                    console.log(data);
                    showAlert('#listData > .row.m-b-25 > .col-md-12', 'success', 'Sửa đồ thị thành công.');
                    $('button[rel="back"]').trigger('click');
                },
                error: function() {
                    // $('button[rel="back"]').trigger('click');
                    showAlert('#editData > div.row.m-b-25 > div', 'danger', 'Có lỗi xảy ra.');
                }
            });
        });

        $('button[rel="addMore"]').click(function(e) {
            e.preventDefault();
            $(e.target).parents('.form-group').before(addMore(index));
            index++;
        });

        $('#pager-prev a').click(function(e) {
            e.preventDefault();
            getList('/reference-graphs?_sort=id:DESC&_start=' + (start -= 10) + '&_limit=10', 'table tbody', appendTable);
            if (start == 0) {
                $('#pager-prev').addClass('disabled');
            }
            $('#pager-next').removeClass('disabled');
        });

        $('#pager-next a').click(function(e) {
            e.preventDefault();
            getList('/reference-graphs?_sort=id:DESC&_start=' + (start += 10) + '&_limit=10', 'table tbody', appendTable);
            $('#pager-prev').removeClass('disabled');
        });
    });

    function deleteRow(e) {
        swal({
            title: "Thông báo",
            text: "Xóa đồ thị tham khảo này?",
            icon: "warning",
            buttons: [
                'Không',
                'Đồng ý'
            ],
            dangerMode: true,
        }).then((isConfirm) => {
            if (isConfirm)
                deleteData('/reference-graphs/' + $(e).attr('data-rel'), '#listData > .row.m-b-25 > .col-md-12', '/reference-graphs?_sort=id:DESC&_start=' + start + '&_limit=10', 'table tbody', appendTable);
        });
    }

    function editRow(e) {
        getData('/reference-graphs/' + $(e).attr('data-rel'), appendEdit);
        $('#listData').hide();
        $('#addData').hide();
        $('#editData').show(300);
    }

    function appendTable(value) {
        var html = '';
        if (value) {
            html += '<tr>';
            html += '   <td>' + value.id + '</td>';
            html += '   <td>' + value.title + '</td>';
            html += '   <td class="text-right">';
            html += '       <button data-rel="' + value.id + '" onclick="editRow(this)" class="p-l-10 p-r-10"><i class="fas fa-edit font-3A3A3A"></i></button>';
            html += '       <button data-rel="' + value.id + '" onclick="deleteRow(this)" class="p-l-10 p-r-10"><i class="fas fa-trash font-3A3A3A"></i></button>';
            html += '   </td>';
            html += '</tr>';
        } else {
            html += '<tr>';
            html += '   <td colspan="6" class="text-center">Không có dữ liệu</td>';
            html += '</tr>';
            $('#pager-next').addClass('disabled');
        }
        return html;
    }

    function appendEdit(value) {
        $('#editData input[type=hidden]#id').val(value.id);
        $('#editData > div.row.m-b-25 > div > div > h2').text('Sửa đồ thị tham khảo ' + value.id);
        $('#editData input[name="title"]').val(value.title);
        if (value.data != null)
            $.each(value.data, function(key, row) {
                $('#editData button[rel="addMore"]').parents('.form-group').before(addMore(key));
                $('#editData #row-' + (key) + ' input[name="label"]').val(row.label);
                $('#editData #row-' + (key) + ' input[name="value"]').val(row.value);
            });
    }

    function addMore(index) {
        var html = '';
        var regex = ` oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\\..*)\\./g, '$1');"`;

        html += '<div class="row form-group chart-row" id="row-' + index + '">';
        html += '   <div class="col-6">';
        html += '       <label for="label" class=" form-control-label d-block">Tên danh mục</label>';
        html += '       <input type="text" name="label" class="form-control" autocomplete="off">';
        html += '   </div>';
        html += '   <div class="col-5">';
        html += '       <label for="value" class=" form-control-label d-block">Giá trị (%)</label>';
        html += '       <input type="text" name="value" class="form-control" autocomplete="off"' + regex + '>';
        html += '   </div>';
        html += '   <div class="col">';
        html += '       <label for="value" class=" form-control-label d-block">&nbsp;</label>';
        html += '       <button class="btn btn-danger" onclick="deleteChartRow(this);" data-id="#row-' + index + '"><i class="fa fa-times"></i></button>';
        html += '   </div>';
        html += '</div>';

        return html;
    }

    function deleteChartRow(e) {
        var id = $(e).attr('data-id');
        $(id).remove();
    }
</script>
<!-- END page-wrapper -->
<?php
include './views/footer.php';
?>
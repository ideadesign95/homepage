<?php
$title = "Quản lý người dùng";
include './views/header.php';
?>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid" id="listData">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Quản lý người dùng</h2>
                        <button class="au-btn au-btn-icon au-btn--green" rel="addData">
                            <i class="zmdi zmdi-plus"></i>Thêm người dùng</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive table--no-card m-b-30">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Vai trò</th>
                                    <th>Hoạt động</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <ul class="pagination float-right">
                        <li class="page-item disabled" id="pager-prev"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                        <li class="page-item" id="pager-next"><a class="page-link" href="#"><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="addData" style="display: none;">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Thêm người dùng</h2>
                        <div class="button-wrap">
                            <button class="au-btn au-btn-icon btn-danger" rel="back">
                                <i class="zmdi zmdi-close"></i>Hủy
                            </button>
                            <button class="au-btn au-btn-icon au-btn--green" rel="saveNewData">
                                <i class="zmdi zmdi-check"></i>Lưu
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col-12">
                                        <label for="roles" class=" form-control-label d-block">Vai trò</label>
                                        <select name="roles" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="username" class=" form-control-label d-block">Tên người dùng</label>
                                        <input type="text" name="username" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-6">
                                        <label for="email" class=" form-control-label d-block">Email</label>
                                        <input type="text" name="email" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="password" class=" form-control-label d-block">Password</label>
                                        <input type="password" name="password" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-6">
                                        <label for="active" class=" form-control-label d-block">Hoạt động</label>
                                        <label class="switch switch-text switch-success switch-pill">
                                            <input type="checkbox" name="active" class="switch-input" checked="true">
                                            <span data-on="On" data-off="Off" class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="editData" style="display: none;">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Sửa câu hỏi</h2>
                        <div class="button-wrap">
                            <button class="au-btn au-btn-icon btn-danger" rel="back">
                                <i class="zmdi zmdi-close"></i>Hủy
                            </button>
                            <button class="au-btn au-btn-icon au-btn--green" rel="saveData">
                                <i class="zmdi zmdi-check"></i>Lưu
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="row form-group">
                                    <input type="hidden" id="id">
                                    <div class="col-12">
                                        <label for="roles" class=" form-control-label d-block">Vai trò</label>
                                        <select name="roles" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="username" class=" form-control-label d-block">Tên người dùng</label>
                                        <input type="text" name="username" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-6">
                                        <label for="email" class=" form-control-label d-block">Email</label>
                                        <input type="text" name="email" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="password" class=" form-control-label d-block">Password</label>
                                        <input type="password" name="password" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-6">
                                        <label for="active" class=" form-control-label d-block">Hoạt động</label>
                                        <label class="switch switch-text switch-success switch-pill">
                                            <input type="checkbox" name="active" class="switch-input" checked="true">
                                            <span data-on="On" data-off="Off" class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->
</div>
<script>
    var index = 1;
    var start = 0;
    $(document).ready(function() {
        activeMenu('/admin/users');
        getList('/users?_start=' + start + '&_limit=10', 'table tbody', appendTable);
        $('#listData').show();
        $('#addData').hide();
        $('#editData').hide();

        $('button[rel="addData"]').click(function(e) {
            e.preventDefault();
            getRoles("#addData select[name='roles']");
            $('#listData').hide();
            $('#editData').hide();
            $('#addData').show(300);
        });

        $('button[rel="back"]').click(function(e) {
            e.preventDefault();
            $('.imagePreview').attr('src', '');
            $('.audioPreview').attr('src', '');
            $('input[name="audio"]').val('');
            $('input[name="image"]').val('');
            $('.answer-row').remove();
            $('#addData').hide();
            $('#editData').hide();
            $('#listData').show(300);
            getList('/users?_start=' + start + '&_limit=10', 'table tbody', appendTable);
            // $('#pager-prev').addClass('disabled');
            // start = 0;
            // index = 1;
        });

        $('button[rel="saveNewData"]').click(function(e) {
            e.preventDefault();
            const formData = new FormData();

            if (
                $('#addData input[name="username"]').val() == "" ||
                $('#addData input[name="email"]').val() == "" ||
                $('#addData input[name="password"]').val() == "" ||
                $('#addData select[name="roles"]').val() < 1
            ) {
                showAlert('#addData > div.row.m-b-25 > div', 'danger', 'Vui lòng điền đầy đủ thông tin.');
                return;
            }

            formData.append('blocked', !$('#addData input[name="active"').prop('checked'));
            formData.append('confirmed', true);
            formData.append('username', $('#addData input[name="username"]').val());
            formData.append('email', $('#addData input[name="email"]').val());
            formData.append('password', $('#addData input[name="password"]').val());
            formData.append('role', $('#addData select[name="roles"]').val());

            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + getCookie('jwt')
                },
                url: URL_API + '/users',
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data) {
                    console.log(data);
                    showAlert('#listData > .row.m-b-25 > .col-md-12', 'success', 'Thêm câu hỏi thành công.');
                    $('button[rel="back"]').trigger('click');
                },
                error: function() {
                    // $('button[rel="back"]').trigger('click');
                    showAlert('#addData > div.row.m-b-25 > div', 'danger', 'Có lỗi xảy ra.');
                }
            });
        });

        $('button[rel="saveData"]').click(function(e) {
            e.preventDefault();
            const formData = new FormData();

            formData.append('blocked', !$('#editData input[name="active"').prop('checked'));
            formData.append('username', $('#editData input[name="username"]').val());
            formData.append('email', $('#editData input[name="email"]').val());
            if ($('#editData input[name="password"]').val() != '')
                formData.append('password', $('#editData input[name="password"]').val());
            formData.append('role', $('#editData select[name="roles"]').val());

            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + getCookie('jwt')
                },
                url: URL_API + '/users/' + $('#editData input[type=hidden]#id').val(),
                data: formData,
                processData: false,
                contentType: false,
                type: 'PUT',
                success: function(data) {
                    console.log(data);
                    showAlert('#listData > .row.m-b-25 > .col-md-12', 'success', 'Sửa người dùng thành công.');
                    $('button[rel="back"]').trigger('click');
                },
                error: function() {
                    // $('button[rel="back"]').trigger('click');
                    showAlert('#editData > div.row.m-b-25 > div', 'danger', 'Có lỗi xảy ra.');
                }
            });
        });

        $('#pager-prev a').click(function(e) {
            e.preventDefault();
            getList('/users?_start=' + (start -= 10) + '&_limit=10', 'table tbody', appendTable);
            if (start == 0) {
                $('#pager-prev').addClass('disabled');
            }
            $('#pager-next').removeClass('disabled');
        });

        $('#pager-next a').click(function(e) {
            e.preventDefault();
            getList('/users?_start=' + (start += 10) + '&_limit=10', 'table tbody', appendTable);
            $('#pager-prev').removeClass('disabled');
        });
    });

    function deleteRow(e) {
        swal({
            title: "Thông báo",
            text: "Xóa người dùng này?",
            icon: "warning",
            buttons: [
                'Không',
                'Đồng ý'
            ],
            dangerMode: true,
        }).then((isConfirm) => {
            if (isConfirm)
                deleteData('/users/' + $(e).attr('data-rel'), '#listData > .row.m-b-25 > .col-md-12', '/users?_start=' + start + '&_limit=10', 'table tbody', appendTable);
        });
    }

    function editRow(e) {
        $('#listData').hide();
        $('#addData').hide();
        $('#editData').show(300);
        getData('/users/' + $(e).attr('data-rel'), appendEdit);
    }

    function appendTable(value) {
        var html = '';
        if (value) {
            html += '<tr>';
            html += '   <td>' + value.id + '</td>';
            html += '   <td>' + value.username + '</td>';
            html += '   <td>' + value.email + '</td>';
            html += '   <td>' + value.role.name + '</td>';
            if (value.blocked === true)
                html += '   <td>Không hoạt động</td>';
            else
                html += '   <td>Đang hoạt động</td>';
            html += '   <td class="text-right">';
            html += '       <button data-rel="' + value.id + '" onclick="editRow(this)" class="p-l-10 p-r-10"><i class="fas fa-edit font-3A3A3A"></i></button>';
            html += '       <button data-rel="' + value.id + '" onclick="deleteRow(this)" class="p-l-10 p-r-10"><i class="fas fa-trash font-3A3A3A"></i></button>';
            html += '   </td>';
            html += '</tr>';
        } else {
            html += '<tr>';
            html += '   <td colspan="6" class="text-center">Không có dữ liệu</td>';
            html += '</tr>';
            $('#pager-next').addClass('disabled');
        }
        return html;
    }

    function appendEdit(value) {
        $('#editData input[type=hidden]#id').val(value.id);
        getRoles("#editData select[name='roles']", value.role.id);
        $('#editData > div.row.m-b-25 > div > div > h2').text('Sửa người dùng ' + value.id);
        $('#editData input[name="username"]').val(value.username);
        $('#editData input[name="email"]').val(value.email);
        $('#editData input[name="password"]').val(value.password);
        $('#editData input[name="active"]').val(!value.blocked);
    }

    function PreviewImage(e) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL($(e)[0].files[0]);

        oFReader.onload = function(oFREvent) {
            $('.imagePreview').attr('src', oFREvent.target.result);
        };
        image_onchange = 1;
    };

    function getRoles(selector, selected = 0) {
        $.ajax({
            headers: {
                'Authorization': 'Bearer ' + getCookie('jwt')
            },
            url: URL_API + "/users-permissions/roles",
            type: "GET",
            contentType: 'application/json',
            success: function(result) {
                $(selector).empty();
                $(selector).append('<option value="0">Vui lòng chọn</option>');
                if (result.roles != null && result.roles != '' && result.roles.length > 0) {
                    $.each(result.roles, function(index, value) {
                        if (value != null && value != '')
                            $(selector).append('<option value="' + value.id + '">' + value.name + ': ' + value.description + '</option>');
                    });
                }

                $(selector).val(selected);
            },
            error: function(jqXHR, error, errorThrown) {
                alert("Có lỗi xảy ra!");
                console.log(jqXHR);
            },
        });
    }
</script>
<!-- END page-wrapper -->
<?php
include './views/footer.php';
?>
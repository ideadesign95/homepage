<?php
$title = "Quản lý báo cáo";
include './views/header.php';
?>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid" id="listData">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Quản lý báo cáo</h2>
                        <!-- <button class="au-btn au-btn-icon au-btn--green" rel="addData">
                            <i class="zmdi zmdi-plus"></i>Thêm báo cáo</button> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive table--no-card m-b-30">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Người tạo</th>
                                    <th>Ngày tạo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <ul class="pagination float-right">
                        <li class="page-item disabled" id="pager-prev"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                        <li class="page-item" id="pager-next"><a class="page-link" href="#"><i class="fas fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="addData" style="display: none;">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Thêm báo cáo</h2>
                        <div class="button-wrap">
                            <button class="au-btn au-btn-icon btn-danger" rel="back">
                                <i class="zmdi zmdi-close"></i>Hủy
                            </button>
                            <button class="au-btn au-btn-icon au-btn--green" rel="saveNewData">
                                <i class="zmdi zmdi-check"></i>Lưu
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col-12">
                                        <label for="roles" class=" form-control-label d-block">Vai trò</label>
                                        <select name="roles" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="username" class=" form-control-label d-block">Tên báo cáo</label>
                                        <input type="text" name="username" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-6">
                                        <label for="email" class=" form-control-label d-block">Email</label>
                                        <input type="text" name="email" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="password" class=" form-control-label d-block">Password</label>
                                        <input type="password" name="password" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-6">
                                        <label for="active" class=" form-control-label d-block">Hoạt động</label>
                                        <label class="switch switch-text switch-success switch-pill">
                                            <input type="checkbox" name="active" class="switch-input" checked="true">
                                            <span data-on="On" data-off="Off" class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="editData" style="display: none;">
            <div class="row m-b-25">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-5">Sửa báo cáo</h2>
                        <div class="button-wrap">
                            <button class="au-btn au-btn-icon btn-danger" rel="back">
                                <i class="zmdi zmdi-close"></i>Hủy
                            </button>
                            <button class="au-btn au-btn-icon au-btn--green" rel="saveData">
                                <i class="zmdi zmdi-check"></i>Lưu
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="row form-group">
                                    <input type="hidden" id="id">
                                    <div class="col-12">
                                        <label for="roles" class=" form-control-label d-block">Vai trò</label>
                                        <select name="roles" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="username" class=" form-control-label d-block">Tên báo cáo</label>
                                        <input type="text" name="username" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-6">
                                        <label for="email" class=" form-control-label d-block">Email</label>
                                        <input type="text" name="email" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-6">
                                        <label for="password" class=" form-control-label d-block">Password</label>
                                        <input type="password" name="password" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="col-6">
                                        <label for="active" class=" form-control-label d-block">Hoạt động</label>
                                        <label class="switch switch-text switch-success switch-pill">
                                            <input type="checkbox" name="active" class="switch-input" checked="true">
                                            <span data-on="On" data-off="Off" class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->
</div>
<script>
    var index = 1;
    var start = 0;
    $(document).ready(function() {
        activeMenu('/admin/forms');
        getList('/forms?_sort=id:DESC&_start=' + start + '&_limit=10', 'table tbody', appendTable);
        $('#listData').show();
        $('#addData').hide();
        $('#editData').hide();

        $('button[rel="addData"]').click(function(e) {
            e.preventDefault();
            $('#listData').hide();
            $('#editData').hide();
            $('#addData').show(300);
        });

        $('button[rel="back"]').click(function(e) {
            e.preventDefault();
            $('.imagePreview').attr('src', '');
            $('.audioPreview').attr('src', '');
            $('input[name="audio"]').val('');
            $('input[name="image"]').val('');
            $('.answer-row').remove();
            $('#addData').hide();
            $('#editData').hide();
            $('#listData').show(300);
            getList('/forms?_sort=id:DESC&_start=' + start + '&_limit=10', 'table tbody', appendTable);
            // $('#pager-prev').addClass('disabled');
            // start = 0;
            // index = 1;
        });

        $('button[rel="saveNewData"]').click(function(e) {
            e.preventDefault();

        });

        $('button[rel="saveData"]').click(function(e) {
            e.preventDefault();

        });

        $('#pager-prev a').click(function(e) {
            e.preventDefault();
            getList('/forms?_sort=id:DESC&_start=' + (start -= 10) + '&_limit=10', 'table tbody', appendTable);
            if (start == 0) {
                $('#pager-prev').addClass('disabled');
            }
            $('#pager-next').removeClass('disabled');
        });

        $('#pager-next a').click(function(e) {
            e.preventDefault();
            getList('/forms?_sort=id:DESC&_start=' + (start += 10) + '&_limit=10', 'table tbody', appendTable);
            $('#pager-prev').removeClass('disabled');
        });
    });

    function deleteRow(e) {
        swal({
            title: "Thông báo",
            text: "Xóa báo cáo này?",
            icon: "warning",
            buttons: [
                'Không',
                'Đồng ý'
            ],
            dangerMode: true,
        }).then((isConfirm) => {
            if (isConfirm)
                deleteData('/forms/' + $(e).attr('data-rel'), '#listData > .row.m-b-25 > .col-md-12', '/forms?_sort=id:DESC&_start=' + start + '&_limit=10', 'table tbody', appendTable);
        });
    }

    function editRow(e) {
        getData('/forms/' + $(e).attr('data-rel'), (response) => {
            let data = response.data;
            setLocalStorage('step1', JSON.stringify(data.step1));
            setLocalStorage('step2', JSON.stringify(data.step2));
            setLocalStorage('step3', JSON.stringify(data.step3));
            openInNewTab('/?form=' + $(e).attr('data-rel'));
        });
        // $('#listData').hide();
        // $('#addData').hide();
        // $('#editData').show(300);
    }

    function appendTable(value) {
        var html = '';
        if (value) {
            html += '<tr>';
            html += '   <td>' + value.id + '</td>';
            html += '   <td>' + value.user_id.username + '</td>';
            html += '   <td>' + formatDate(value.created_at) + '</td>';
            html += '   <td class="text-right">';
            html += '       <button data-rel="' + value.id + '" onclick="editRow(this)" class="p-l-10 p-r-10"><i class="fas fa-edit font-3A3A3A"></i></button>';
            html += '       <button data-rel="' + value.id + '" onclick="deleteRow(this)" class="p-l-10 p-r-10"><i class="fas fa-trash font-3A3A3A"></i></button>';
            html += '   </td>';
            html += '</tr>';
        } else {
            html += '<tr>';
            html += '   <td colspan="6" class="text-center">Không có dữ liệu</td>';
            html += '</tr>';
            $('#pager-next').addClass('disabled');
        }
        return html;
    }

    // function appendEdit(value) {
    //     $('#editData input[type=hidden]#id').val(value.id);
    //     $('#editData > div.row.m-b-25 > div > div > h2').text('Sửa báo cáo ' + value.id);
    //     $('#editData input[name="username"]').val(value.username);
    //     $('#editData input[name="email"]').val(value.email);
    //     $('#editData input[name="password"]').val(value.password);
    //     $('#editData input[name="active"]').val(!value.blocked);
    // }
</script>
<!-- END page-wrapper -->
<?php
include './views/footer.php';
?>
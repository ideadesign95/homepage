<?php

function callAPI($method, $url, $headers = [], $data = false, $is_json = true)
{
    try {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        if (is_array($headers)) {
            foreach ($headers as $key => $value) {
                curl_setopt($curl, CURLOPT_HTTPHEADER, array($value));
            }
        }
        if ($method == 'POST' && $is_json == true) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        curl_close($curl);
    } catch (\Exception $e) {
        return false;
    }

    return $result;
}

function returnJson($data = [], $isDie = true)
{
    echo json_encode($data);
    if ($isDie === true) die;
}

function stripText($str)
{
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);

    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);

    $str = preg_replace('/\s+/', '-', $str);
    $str = preg_replace('/-+/', '-', $str);
    $str = preg_replace('/[^A-Za-z0-9\-]/', '', $str);

    return $str;
}

function formatValue($key, $value)
{
    if (is_numeric($value) && !in_array($key, array("phone", "phone_contact", "receive_phone", "is_hls", "name", "description", "name_en", "description_en"))) {    // int + float
        if (is_int($value))
            $value = intval($value);
        else
            $value = floatval($value);
    }

    if (is_numeric($value) && (strpos($key, 'is_') !== false || in_array($key, ['active']))) {  // boolean
        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    if ($value != null && is_string($value) && strlen($value) > 0 && in_array($key, array("image", "image_jp", "video")) && strpos($value, "http") === false) {
        $value = getenv('URL_STATIC') . "" . $value;
    }

    if (is_null($value)) {
        $value = '';
    }

    return $value;
}

?>
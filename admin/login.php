<?php
if (isset($_COOKIE['jwt']) && isset($_COOKIE['user']) && isset($_COOKIE['permissions']))
    header("Location: /admin/index");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="iEnglish">
    <meta name="author" content="iEnglish">
    <meta name="keywords" content="iEnglish">

    <!-- Title Page-->
    <title>Đăng nhập</title>

    <!-- Fontfaces CSS-->
    <link href="./css/font-face.css" rel="stylesheet" media="all">
    <link href="./vendor/font-awesome-4.7//css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="./vendor/font-awesome-5//css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="./vendor/mdi-font//css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="./vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="./vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="./vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="./vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="./vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="./vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="./vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="./vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="./css/theme.css" rel="stylesheet" media="all">
    <link rel="shortcut icon" href="http://enerteam.org/wp-content/themes/fokatech/favicon.png">

</head>

<body class="animsition" style="overflow: scroll;">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="images/icon/logo.png">
                            </a>
                        </div>
                        <div class="login-form">
                            <form action="" method="post" id="login_form">
                                <div class="form-group">
                                    <div class="alert alert-danger responseAlert alert-dismissible fade show" role="alert" style="display: none;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Tên đăng nhập / Email</label>
                                    <input class="au-input au-input--full" type="text" name="identifier" placeholder="Tên đăng nhập / Email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                                </div>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Đăng nhập</button>
                                <a href="/" class="font-green"><i class="fas fa-arrow-left"></i> Quay về trang chủ</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="./vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="./vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="./vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="./vendor/slick/slick.min.js">
    </script>
    <script src="./vendor/wow/wow.min.js"></script>
    <script src="./vendor/animsition/animsition.min.js"></script>
    <script src="./vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="./vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="./vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="./vendor/circle-progress/circle-progress.min.js"></script>
    <script src="./vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="./vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="./vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <?php include_once('./views/const.js.php') ?>
    <script src="./js/main.js"></script>
    <script src="./js/script.js"></script>

    <script>
        // jQuery codes
        $(document).ready(function() {
            clearResponse();

            // trigger when login form is submitted
            $(document).on('submit', '#login_form', function(e) {
                e.preventDefault();
                // get form data
                var login_form = $(this);
                var form_data = JSON.stringify(login_form.serializeObject());

                $.ajax({
                    url: URL_API + "/auth/local",
                    type: "POST",
                    contentType: 'application/json',
                    data: form_data,
                    success: function(result) {
                        // store jwt to cookie
                        setCookie("jwt", result.jwt, 2);
                        setCookie("user", result.user.username, 2);
                        setCookie("result", result.user.id, 2);
                        setLocalStorage("login_data", form_data);

                        if (result.user.role.name === ROLE_ADMIN && result.user.role.type === ROLE_ADMIN.toLowerCase()) {
                            setCookie("permissions", PERMISSION_ADMIN, 2);
                        } else {
                            setCookie("permissions", PERMISSION_USER, 2);
                        }

                        // redirect to homepage
                        $('#response').html("<div class='alert alert-success'>Successful login.</div>");
                        showHomePage();
                    },
                    error: function(xhr, textStatus) {
                        clearResponse();
                        responseAlert(xhr.status + '. Sai thông tin đăng nhập.');
                        localStorage.removeItem('login_data');
                    }
                    // error response will be here
                });

                return false;
            });

            // trigger to show home page will be here
            function showHomePage() {
                window.location.replace("/admin/index");
            }
        });
    </script>
</body>

</html>
<!-- end document-->
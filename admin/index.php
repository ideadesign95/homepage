<?php
$title = "Dashboard";
include './views/header.php';
?>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Welcome <?php echo $_COOKIE['user'] ?></strong>
                        </div>
                        <div class="card-body">
                            <p>
                                Trung tâm Nghiên cứu và Phát triển về Tiết kiệm Năng lượng – gọi tắt là ENERTEAM, là tổ chức khoa học công nghệ được thành lập đầu tiên ở Việt Nam chuyên về tiết kiệm năng lượng và quản lý tài nguyên.
                                Là tổ chức khoa học công nghệ được thành lập đầu tiên ở Việt Nam chuyên về tiết kiệm năng lượng và quản lý tài nguyên.Tiền thân là tổ chức Trans Energ – Pháp (hoạt động từ năm 1995), đến năm 1999 ENERTEAM đã chính thức trở thành đơn vị tư vấn độc lập với tên gọi Trung tâm Nghiên cứu và Phát triển về Tiết kiệm Năng lượng – gọi tắt là ENERTEAM, hoạt động theo Luật Khoa học và Công nghệ, giấy phép hoạt động do Sở Khoa học và Công nghệ Tp. Hồ Chí Minh cấp.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->
<!-- END PAGE CONTAINER-->
</div>
<script>
    $(document).ready(function() {
        activeMenu('/admin/index');

    });
</script>
<!-- END page-wrapper -->
<?php
include './views/footer.php';
?>
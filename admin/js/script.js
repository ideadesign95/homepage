// function to make form values to json format
$.fn.serializeObject = function(){
 
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function clearResponse(){
    $('.responseAlert').text('');
    $('.responseAlert').hide();
};

function responseAlert(string){
    $('.responseAlert').text(string);
    $('.responseAlert').show(500);
};

function getCookie(cname){
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' '){
            c = c.substring(1);
        }
 
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

function logout(){
    document.cookie = "jwt=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "user=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "permissions=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "result=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    localStorage.removeItem('login_data');
    window.location.reload();
};

function getList(url, selector, callback){
    $.ajax({
        headers: {'Authorization': 'Bearer ' + getCookie('jwt')},
        url: URL_API + url,
        type : "GET",
        contentType : 'application/json',
        success : function(result){
            $(selector).empty();
            if(result != null && result != ''){
                $.each(result, function(index, value) {
                    if(value != null && value != '')
                        $(selector).append(callback(value));
                });
            }
            else
                $(selector).append(callback(null));
        },
        error: function(jqXHR,error, errorThrown) {  
            alert("Có lỗi xảy ra!");
            console.log(jqXHR);
       },
    });
};

function showAlert(selector, type, message){
    if(type == 'success')
    {
        $(selector).append('<div class="alert alert-success alert-dismissible fade show m-t-10 m-b-10"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Thành công!</strong> ' + message + '</div>');
        closeAlert(selector, 7000);
    }
    else if(type == 'danger')
    {
        $(selector).append('<div class="alert alert-danger alert-dismissible fade show m-t-10 m-b-10"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Lỗi!</strong> ' + message + '</div>');
        closeAlert(selector, 7000);
    }
};

function closeAlert(selector, time){
    setTimeout(function() {
        $(selector + ' .alert.alert-dismissible').remove();
    }, time);
}

function deleteData(url, selector, callback_url, callback_selector, callback_callback){
    $.ajax({
        headers: {'Authorization': 'Bearer ' + getCookie('jwt')},
        url: URL_API + url,
        type : "DELETE",
        contentType : 'application/json',
        success : function(result){
            showAlert(selector, 'success', 'Xóa thành công.');
            getList(callback_url, callback_selector, callback_callback);
        },
        error: function(jqXHR, error, errorThrown) {  
            showAlert(selector, 'danger', 'Có lỗi xảy ra.');
            console.log(jqXHR);
       },
    });
};

function activeMenu(url){
    $('a[href*="' + url + '"]').parent().addClass('active');
};

function getData(url, callback){
    $.ajax({
        headers: {'Authorization': 'Bearer ' + getCookie('jwt')},
        url: URL_API + url,
        type : "GET",
        contentType : 'application/json',
        success : function(result){
            if(result != null && result != ''){
                callback(result);
            }
        },
        error: function(jqXHR,error, errorThrown) {  
            alert("Có lỗi xảy ra!");
            console.log(jqXHR);
       },
    });
};

function appendSelect(data) {
    var html = '';
    if (data) {
        html += '<option value="' + data.id + '">' + data.name + '</option>';
    }

    return html;
}

function setLocalStorage(key, value) {
    localStorage.setItem(key, value);
}

function getLocalStorage(key) {
    return localStorage.getItem(key);
}

function removeLocalStorage(key) {
    localStorage.removeItem(key);
}

function clearLocalStorage() {
    localStorage.clear();
}

function formatDate(time, format = "h:MM:s  dd-mm-yyyy") {
    time = new Date(time);
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var date = time.getDate();
    var hours = time.getHours();
    var minutes = time.getMinutes();
    var seconds = time.getSeconds();
    var add0 = function (t) { return t < 10 ? '0' + t : t }
    var replaceMent = {
        'yyyy': year,
        'mm': add0(month),
        'm': month,
        'dd': add0(date),
        'd': date,
        'hh': add0(hours),
        'h': hours,
        'MM': add0(minutes),
        'M': minutes,
        'ss': add0(seconds),
        's': seconds
    }
    for (var k in replaceMent) {
        format = format.replace(k, replaceMent[k]);
    }
    return format;
}

function openInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}
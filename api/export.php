<?php
// ini_set('display_errors', 1);
// error_reporting(E_ALL);

date_default_timezone_set('Asia/Ho_Chi_Minh');
header("Content-Type: application/json; charset=UTF-8");

include_once("../include/const.php");
// include_once("../include/db.php");
include_once("../include/functions.php");

// $headers = (array) getallheaders();
// if (!empty($headers['Authorization'])) {
//     $checkToken = callAPI('GET', URL_API . "/users/me", ["Authorization: " . $headers['Authorization']]);
//     if ($checkToken !== false) {
//         $checkToken = json_decode($checkToken, true);
//         if (count($checkToken) > 0) {
//             $uid = intval($checkToken['id']);
//         }
//     }
// }

$data = json_decode(file_get_contents('php://input'), true);
if (!$data)
    returnJson([
        'result' => false,
        'message' => "Dữ liệu không hợp lệ."
    ]);
$uid = empty($data['uid']) ? 0 : intval($uid);

require_once('../bootstrap.php');
$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($_SERVER['DOCUMENT_ROOT'].'/include/Template-TT-52.docx');

foreach ($data['step1'] as $key => $value) {
    $templateProcessor->setValue($key, $value);
    if (strpos($key, '-ngay') !== false) {
        $parts = explode('-', $value);
        $keyNgay = "${key}-ngay";
        $keyThang = "${key}-thang";
        $keyNam = "${key}-nam";
        $templateProcessor->setValues([
            $keyNgay => $parts[0],
            $keyThang => $parts[1],
            $keyNam => $parts[2]
        ]);
    }
}
$templateProcessor->setValue('ngay-thang-nam', date("\\n\g\à\y d \\t\h\á\\n\g m \\n\ă\m Y"));
foreach ($data['step2'] as $key => $value) {
    $value = formatValue($key, $value);
    if (strpos($key, 'check-') !== false) {
        $check = $value == 'true' ? "☒ Có ☐ Không" : "☐ Có ☒ Không";
        $templateProcessor->setValue($key, $check);
    } else {
        $check = "☐ Có ☒ Không";
        if (!empty($value) && $value != 0)
            $check = "☒ Có ☐ Không";
        $templateProcessor->setValue($key, rtrim(rtrim((string)number_format(floatval($value), 2, ".", ","),"0"),"."));
        $templateProcessor->setValue("check-${key}", $check);
    }
}
foreach ($data['step4'] as $key => $value) {
    $templateProcessor->setValue($key, rtrim(rtrim((string)number_format(floatval($value), 2, ".", ","),"0"),"."));
}
foreach ($data['step3'] as $key => $value) {
    $value = formatValue($key, $value);
    $isCa = false;
    $keyTmp = "";
    $loaiSanPham = "";
    $sanLuong = "";
    $loaiSanPhamCa = 'loai-san-pham-ca-da-tron-';
    $loaiSanPhamTom = 'loai-san-pham-tom-';
    $sanLuongCa = "san-luong-ca-";
    $sanLuongTom = "san-luong-tom-";
    if (strpos($key, $loaiSanPhamCa) !== false) {
        $loaiSanPham = $loaiSanPhamCa;
        $sanLuong = $sanLuongCa;
        $isCa = true;
    } else if (strpos($key, $loaiSanPhamTom) !== false) {
        $loaiSanPham = $loaiSanPhamTom;
        $sanLuong = $sanLuongTom;
        $isCa = false;
    } else if (strpos($key, $sanLuongCa) !== false) {
        $isCa = true;
    } else if (strpos($key, $sanLuongTom) !== false) {
        $isCa = false;
    }

    $index = intval(preg_replace("/[^0-9]/","",$key));
    if ($isCa === true && $index > 10) {
        if (strpos($key, $loaiSanPhamCa) !== false) {
            $keyTmp = "SAN_PHAM_CA_KHAC_${index}";
            $keyOfVal = "${loaiSanPhamCa}${index}";
        } else {
            $keyTmp = "KG_SAN_PHAM_CA_KHAC_${index}";
            $keyOfVal = "${sanLuongCa}${index}";
        }
        $templateProcessor->setValue($keyTmp, rtrim(rtrim((string)number_format(floatval($data['step3'][$keyOfVal]), 2, ".", ","),"0"),"."));
    } else if ($isCa === false && $index > 5) {
        if (strpos($key, $loaiSanPhamTom) !== false) {
            $keyTmp = "SAN_PHAM_TOM_KHAC_${index}";
            $keyOfVal = "${loaiSanPhamTom}${index}";
        } else {
            $keyTmp = "KG_SAN_PHAM_TOM_KHAC_${index}";
            $keyOfVal = "${sanLuongTom}${index}";
        }
        $templateProcessor->setValue($keyTmp, rtrim(rtrim((string)number_format(floatval($data['step3'][$keyOfVal]), 2, ".", ","),"0"),"."));
    } else {
        switch (floatval($value)) {
            case CA_PHI_LE_15:
                if ($isCa)
                    $keyTmp = "CA_PHI_LE_15";
                break;
            case CA_PHI_LE_25:
                $keyTmp = "CA_PHI_LE_25";
                break;
            case CA_PHI_LE_35:
                $keyTmp = "CA_PHI_LE_35";
                break;
            case CA_NGUYEN_CON_15:
                $keyTmp = "CA_NGUYEN_CON_15";
                break;
            case CA_NGUYEN_CON_25:
                $keyTmp = "CA_NGUYEN_CON_25";
                break;
            case CA_NGUYEN_CON_35:
                $keyTmp = "CA_NGUYEN_CON_35";
                break;
            case CA_TAM_GIA_VI:
                $keyTmp = "CA_TAM_GIA_VI";
                break;
            case SAN_PHAM_BLOCK_CAP_DONG:
                $keyTmp = "SAN_PHAM_BLOCK_CAP_DONG";
                break;
            case CA_CAT_KHUC_CAP_DONG:
                $keyTmp = "CA_CAT_KHUC_CAP_DONG";
                break;
            case PHU_PHAM_CAP_DONG:
                $keyTmp = "PHU_PHAM_CAP_DONG";
                break;
            case TOM_TUOI:
                $keyTmp = "TOM_TUOI";
                break;
            case TOM_HAP:
                $keyTmp = "TOM_HAP";
                break;
            case TOM_NOBASHI:
                $keyTmp = "TOM_NOBASHI";
                break;
            case TOM_TEMPURA:
                $keyTmp = "TOM_TEMPURA";
                break;
            case TOM_BLOCK_CAP_DONG:
                $keyTmp = "TOM_BLOCK_CAP_DONG";
                break;
            default:
                break;
        }
        $templateProcessor->setValue($keyTmp, rtrim(rtrim((string)number_format(floatval($data['step3']["${sanLuong}${index}"]), 2, ".", ","),"0"),"."));
    }
}
$templateProcessor->setValue('slqdCa', rtrim(rtrim((string)number_format(floatval($data['slqd']['slqdCa']), 2, ".", ","),"0"),"."));
$templateProcessor->setValue('slqdTom', rtrim(rtrim((string)number_format(floatval($data['slqd']['slqdTom']), 2, ".", ","),"0"),"."));
$templateProcessor->setValues([
    'CA_PHI_LE_15' => 0,
    'CA_PHI_LE_25' => 0,
    'CA_PHI_LE_35' => 0,
    'CA_NGUYEN_CON_15' => 0,
    'CA_NGUYEN_CON_25' => 0,
    'CA_NGUYEN_CON_35' => 0,
    'CA_TAM_GIA_VI' => 0,
    'SAN_PHAM_BLOCK_CAP_DONG' => 0,
    'CA_CAT_KHUC_CAP_DONG' => 0,
    'PHU_PHAM_CAP_DONG' => 0,
    'TOM_TUOI' => 0,
    'TOM_HAP' => 0,
    'TOM_NOBASHI' => 0,
    'TOM_TEMPURA' => 0,
    'TOM_BLOCK_CAP_DONG' => 0,
]);

$templateProcessor->setValue('suat-tieu-hao-nang-luong-ky-truoc', '..........');
$templateProcessor->setValue('chenh-lech-sec', '..........');
$templateProcessor->setValue('thanh-pho', '..........');

$filename = date('YmdHis').'-'.stripText($data['step1']['ten-co-so']);
$path = "../data/".$uid."/".date('Y')."/".date("m");
if (!is_dir($path)) {
    mkdir($path, 0777, true);
}
$templateProcessor->saveAs("${path}/${filename}.docx");
$path = str_replace('.', '', $path);

// $formId = intval($data['form']);
// if ($uid !== 0) {
//     $tmp['user_id'] = $uid;
//     $tmp['created_by'] = $uid;
//     $tmp['updated_by'] = $uid;
//     $tmp['data'] = $data;
//     $data = json_encode($tmp, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
//     if ($formId === 0) {
//         $res = callAPI("POST", URL_API . "/forms", ["Authorization: " . $headers['Authorization']], [
//             'data' => $data
//         ], false);
//     } else {
//         $res = callAPI("PUT", URL_API . "/forms/${formId}", ["Authorization: " . $headers['Authorization']], [
//             'data' => $data
//         ], false);
//     }
//     // $db = new Database();
//     // if ($formId === 0) { // create
//     //     $lastId = true;
//     //     $query = "
//     //         INSERT INTO forms
//     //         SET
//     //             user_id = ${uid}, 
//     //             data = '${data}',
//     //             published_at = CURRENT_TIMESTAMP,
//     //             created_at = CURRENT_TIMESTAMP,
//     //             updated_at = CURRENT_TIMESTAMP,
//     //             created_by = ${uid},
//     //             updated_by = ${uid}
//     //     ";
//     // } else { // update
//     //     $lastId = false;
//     //     $query = "
//     //         UPDATE forms
//     //         SET data = '${data}',
//     //             updated_at = CURRENT_TIMESTAMP,
//     //             updated_by = ${uid}
//     //         WHERE id = ${formId}
//     //     ";
//     // }
//     // $stmt = executeRawQuery($db, $query, null, $lastId);
// }

returnJson([
    'result' => true,
    "message" => "Tạo báo cáo thành công",
    "url" => "${path}/${filename}.docx",
]);

?>
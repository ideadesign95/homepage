<?php
$title = "Step 1";
include_once('./views/header.php');
?>

<div class="main__content">
    <?php include_once("./views/top.php"); ?>

    <div class="content mt-5">
        <h3 class="main__tit">Nhập Thông Tin Chung</h3>
        <div class="form__content pt-5">
            <form id="form-step-1" autocomplete="off">
                <div class="submit__cover" id="new-form-cover" style="display: none; margin-top: 0; margin-bottom: 30px">
                    <button class="next" id="button-new-form" style="width: 200px;">Tạo form mới</button>
                </div>
                <div class="form-group">
                    <label>Tên cơ sở<span>*</span></label>
                    <input type="text" class="input__style" placeholder="Nhập tên cơ sở" name="ten-co-so" data-required>
                    <div class="hint"></div>
                </div>
                <div class="form-group">
                    <label>Mã số thuế<span></span></label>
                    <input type="text" class="input__style" placeholder="Nhập mã số thuế" title="Nhập mã số thuế" name="ma-so-thue" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                </div>
                <div class="form-group">
                    <label>Địa chỉ<span></span></label>
                    <input type="text" class="input__style" placeholder="Nhập địa chỉ" name="dia-chi">
                </div>
                <div class="form-group">
                    <label>Người đại diện<span>*</span></label>
                    <input type="text" class="input__style" placeholder="Nhập tên người đại diện" name="nguoi-dai-dien" data-required>
                    <div class="hint"></div>
                </div>
                <div class="form-group">
                    <label>Chức vụ, phòng ban<span></span></label>
                    <input type="text" class="input__style" placeholder="Vd: Trưởng phòng" name="vi-tri">
                </div>
                <div class="form-group">
                    <label>Điện thoại<span>*</span></label>
                    <input type="text" class="input__style" placeholder="Nhập số điện thoại" name="so-dien-thoai" data-required oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                    <div class="hint"></div>
                </div>
                <div class="form-group">
                    <label>Email<span>*</span></label>
                    <input type="email" class="input__style" placeholder="Vd: enerteam@enerteam.org" name="email" data-required>
                    <div class="hint"></div>
                </div>
                <div class="form-group">
                    <label>Báo cáo số<span></span></label>
                    <input type="text" class="input__style" placeholder="Báo cáo số" name="bao-cao-so">
                </div>
                <div class="form-group choose-date">
                    <label>Thời gian tổng hợp<span>*</span></label>
                    <div class="datepicker">
                        <div class="datepicker__input">
                            <label>Từ</label>
                            <input id="datepicker1" type="text" autocomplete="off" name="tu-ngay" data-required>
                        </div>
                        <div class="datepicker__input">
                            <label>Đến</label>
                            <input id="datepicker2" type="text" autocomplete="off" name="den-ngay" data-required>
                        </div>
                    </div>
                    <div class="hint" style="display: none;">Ngày bắt đầu không được lớn hơn ngày kết thúc</div>
                </div>
                <div class="submit__cover step1-btn">
                    <button class="back">QUAY LẠI</button>
                    <button class="next" id="button-next">Tiếp tục</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        activeMenu('/index');
        activeStep(1);

        var dataLocal = getLocalStorage(STEP1_KEY);
        if (dataLocal != undefined && dataLocal != '') {
            populateForm('#form-step-1', dataLocal);
            $('#new-form-cover').show();
        }

        var form = $('#form-step-1');
        $('#button-next').click((e) => {
            var check = 0;
            e.preventDefault();
            if (!checkNull('#form-step-1'))
                return false;

            var email = $('input[name="email"]').val();
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if (!email.match(mailformat)) {
                $('input[name="email"]').parents('.form-group').addClass('error');
                $('input[name="email"]').next('.hint').text('Vui lòng nhập email đúng định dạng');
                return false;
            }

            // var phoneNum = $('input[name="so-dien-thoai"]').val().length;
            // if (phoneNum < 10 || phoneNum > 11) {
            //     $('input[name="so-dien-thoai"]').parents('.form-group').addClass('error');
            //     $('input[name="so-dien-thoai"]').next('.hint').text('Vui lòng nhập số điện thoại đúng định dạng');
            //     return false;
            // }

            setLocalStorage(STEP1_KEY, JSON.stringify($("#form-step-1").serializeObject()));
            window.location.href = './step-2' + window.location.search;
        });

        $('button.back').click((e) => {
            e.preventDefault();
        });

        $('#button-new-form').click((e) => {
            e.preventDefault();
            removeLocalStorage(STEP1_KEY);
            removeLocalStorage(STEP2_KEY);
            removeLocalStorage(STEP3_KEY);
            $('input').val('');
            $('#new-form-cover').hide(200);
        });
    });
</script>

<?php
include_once('./views/footer.php');
?>
jQuery(document).ready(function ($) {


    $(window).on('resize', function () {
        setHeader();
    });

    $(document).on('scroll', function () {
        setHeader();
    });

    // menu fixed js code
    function setHeader() {
        if ($(window).scrollTop() > 100) {
            $('header').addClass('scrolled');
        } else {
            $('header').removeClass('scrolled');
        }
    }

    $("#datepicker1").datepicker({
        dateFormat: "dd-mm-yy", 
        duration: "fast",
        onSelect: function (selected) {
            var dateParts = selected.split("-");
            var dt = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
            dt.setDate(dt.getDate() + 1);
            $("#datepicker2").datepicker("option", "minDate", dt);
        }
    });
    $("#datepicker2").datepicker({
        dateFormat: "dd-mm-yy", 
        duration: "fast",
        onSelect: function (selected) {
            var dateParts = selected.split("-");
            var dt = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
            dt.setDate(dt.getDate() - 1);
            $("#datepicker1").datepicker("option", "maxDate", dt);
            console.log(selected);
        }
    });

    /*  about timeline on mb*/
    $(".js_chooseme").on("click touch", function () {
        $(this)
            .parent()
            .find(".js_showarchive")
            .slideToggle(300);
        $(this).toggleClass("js_active");

        if ($(this).parent('div').hasClass('mb-step-addstyle'))
            $(this).parent('div').removeClass('mb-step-addstyle');
        else
            $(this).parent('div').addClass('mb-step-addstyle');


    });

    $('.modal').on('hide.bs.modal', function (e) {
        $('.modal__dangnhap .nav-link').removeClass('active');
        $('.modal__dangnhap .tab-content .tab-pane').removeClass('show active');
        $('.modal__dangnhap .tit-dangki').removeClass('d-none');
        $('.modal__dangnhap .tit-dangnhap').removeClass('d-none');

    })

    $(".btn__account.signup").on('click', function () {
        $('.modal__dangnhap .modal-dialog .right .nav-tabs li:first-of-type a').addClass('active');
        $('.modal__dangnhap .tab-content .tab-pane:first-of-type').addClass('show active');
        $('.modal__dangnhap .tit-dangki').addClass('d-none');


    });
    $(".btn__account.login").on('click', function () {
        $('.modal__dangnhap .modal-dialog .right .nav-tabs li:last-of-type a').addClass('active');
        $('.modal__dangnhap .tab-content .tab-pane:last-of-type').addClass('show active');
        $('.modal__dangnhap .tit-dangnhap').addClass('d-none');

    });
    /* end check dangnhap*/

    $('input:radio[data-toggle]').change(function () {
        var dataToggle = $(this).attr('data-toggle');
        var dataToggleValue = $(this).attr('data-toggle-value');
        if (dataToggle.toLowerCase() == 'yes') {
            $('[data-toggle=' + dataToggleValue + ']').slideDown(300);
        } else {
            $('[data-toggle=' + dataToggleValue + ']').slideUp(300);
        }
    });

    // $('input:radio[data-toggle="no"]').change(function () {
    //     $(this).prop('checked', true);
    //     var dataToggle = $(this).attr('data-toggle-value');
    //     $('[data-toggle=' + dataToggle + ']').slideUp(300);
    // });

});
<?php
$title = "Step 2";
include_once('./views/header.php');
?>

<div class="main__content">
    <?php include_once("./views/top.php"); ?>

    <div class="content mt-5">
        <h3 class="main__tit">Số Liệu Tiêu Thụ Điện & Thông Số Sản Xuất</h3>
        <div class="form__content pt-5">
            <form id="form-step-2">
                <div class="form-group btn-radio">
                    <label>1. Cơ sở có hoạt động sản xuất, chế biến sản phẩm, dịch vụ ngoài chế biến tôm và cá da trơn không (ví dụ: chế biến hải sản, bán đá cây, thuê kho, ... )</label>
                    <p class="custom-radio">
                        <input type="radio" id="check-loai-hoat-dong-yes" name="check-loai-hoat-dong" value="true" data-toggle="yes" data-toggle-value="1a" data-required>
                        <label for="check-loai-hoat-dong-yes">Có</label>
                    </p>
                    <p class="custom-radio">
                        <input type="radio" id="check-loai-hoat-dong-no" name="check-loai-hoat-dong" value="false" data-toggle="no" data-toggle-value="1a" data-required>
                        <label for="check-loai-hoat-dong-no">Không</label>
                    </p>

                    <div class="form-group js_showarchive" data-toggle="1a">
                        <label>1a) Loại hoạt động sản xuất, chế biến sản phẩm, dịch vụ ngoài chế biên tôm và cá da trơn<span class="hint">Không được để trống</span></label>
                        <input type="text" class="input__style" placeholder="Loại hoạt động" name="loai-hoat-dong">
                    </div>
                </div>

                <div class="form-group add__sub__input">
                    <label>2. Tổng điện năng tiêu thụ của cơ sở <span>*</span></label>
                    <input type="text" class="input__style" placeholder="Nhập tổng điện năng tiêu thụ của cơ sở trong vòng 1 năm theo số liệu đồng hồ điện lực" title="Nhập tổng điện năng tiêu thụ của cơ sở trong vòng 1 năm theo số liệu đồng hồ điện lực" name="tong-dien-nang-tieu-thu" data-required oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">kWh</span>
                    <div class="hint"></div>
                </div>

                <div class="form-group btn-radio">
                    <label>3. Có thu thập số liệu điện tiêu thụ các khu vực không thuộc phạm vi đánh giá (văn phòng, nhà ăn…) không?</label>
                    <p class="custom-radio">
                        <input type="radio" id="check-dien-tieu-thu-yes" name="check-dien-tieu-thu" value="true" data-toggle-value="3a" data-required>
                        <label for="check-dien-tieu-thu-yes">Có</label>
                    </p>
                    <p class="custom-radio">
                        <input type="radio" id="check-dien-tieu-thu-no" name="check-dien-tieu-thu" value="false" data-toggle-value="3b" data-required>
                        <label for="check-dien-tieu-thu-no">Không</label>
                    </p>
                </div>
                <div class="form-group js_showarchive add__sub__input" data-toggle="3a">
                    <label>3a) Điện tiêu thụ (số liệu thu thập) cho khu vực không thuộc phạm vi đánh giá (văn phòng, nhà ăn, …)</label>
                    <input type="text" class="input__style" placeholder="Nhập điện năng tiêu thụ của khu vực không thuộc phạm vi đánh giá trong 1 năm theo số liệu đồng hồ con" title="Nhập điện năng tiêu thụ của khu vực không thuộc phạm vi đánh giá (văn phòng, nhà ăn, …) trong vòng 1 năm theo số liệu đồng hồ con" name="dien-tieu-thu" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">kWh</span>
                    <div class="hint"></div>
                </div>
                <div class="form-group js_showarchive add__sub__input" data-toggle="3b">
                    <label>3b) Điện tiêu thụ (ước tính bằng 3% tổng điện tiêu thụ) cho khu vực không thuộc phạm vi đánh giá (văn phòng, nhà ăn, …)<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập điện tiêu thụ (ước tính bằng 3% tổng điện tiêu thụ)" title="Nhập điện năng tiêu thụ (ước tính bằng 3% tổng điện tiêu thụ) của khu vực không thuộc phạm vi đánh giá (văn phòng, nhà ăn, …) trong vòng 1 năm theo số liệu đồng hồ con" name="dien-tieu-thu-uoc-tinh" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">kWh</span>
                </div>

                <div class="form-group add__sub__input">
                    <label>4. Khối lượng đá cây/đá vẩy bán ra<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập khối lượng đá cây/đá vẩy bán ra trong vòng 1 năm" title="Nhập khối lượng đá cây/đá vẩy bán ra trong vòng 1 năm" name="khoi-luong-da-ban-ra" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">Tấn</span>
                </div>

                <div class="form-group add__sub__input">
                    <label>5. Sản lượng cho thuê kho lạnh<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập sản lượng cho thuê kho lạnh trong vòng 1 năm" title="Nhập sản lượng cho thuê kho lạnh trong vòng 1 năm" name="san-luong-cho-thue-kho-lanh" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">Tấn</span>
                </div>
                <div class="form-group add__sub__input">
                    <label>6. Thời gian cho thuê kho lạnh<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập số ngày cho thuê kho lạnh trong 1 năm. Có thể sử dụng số liệu trung bình" title="Nhập số ngày cho thuê kho lạnh trong 1 năm. Có thể sử dụng số liệu trung bình" name="thoi-gian-cho-thue-kho-lanh" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">Ngày</span>
                </div>

                <div class="form-group add__sub__input">
                    <label>7. Số tiền cho thuê kho lạnh<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập số tiền cho thuê kho lạnh trong vòng 1 năm" title="Nhập số tiền cho thuê kho lạnh trong vòng 1 năm" name="so-tien-cho-thue-kho-lanh" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">VND</span>
                </div>

                <div class="form-group add__sub__input">
                    <label>8. Khối lượng đá cây/đá vẩy mua vào<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập khối lượng đá cây/đá vẩy mua vào trong 1 năm" title="Nhập khối lượng đá cây/đá vẩy mua vào trong 1 năm" name="khoi-luong-da-mua-vao" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">Tấn</span>
                </div>

                <div class="form-group add__sub__input">
                    <label>9. Sản lượng thuê kho lạnh<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập sản lượng thuê kho lạnh trong vòng 1 năm" title="Nhập sản lượng thuê kho lạnh trong vòng 1 năm" name="san-luong-thue-kho-lanh" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">Tấn</span>
                </div>
                <div class="form-group add__sub__input">
                    <label>10. Thời gian thuê kho lạnh<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập số ngày thuê kho lạnh trong 1 năm. Có thể sử dụng số liệu trung bình" title="Nhập số ngày thuê kho lạnh trong 1 năm. Có thể sử dụng số liệu trung bình" name="thoi-gian-thue-kho-lanh" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">Ngày</span>
                </div>

                <div class="form-group add__sub__input">
                    <label>11. Số tiền thuê kho lạnh<span class="hint">Không được để trống</span></label>
                    <input type="text" class="input__style" placeholder="Nhập số tiền thuê kho lạnh trong vòng 1 năm" title="Nhập số tiền thuê kho lạnh trong vòng 1 năm" name="so-tien-thue-kho-lanh" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                    <span class="sub">VND</span>
                </div>

                <!--btn-->
                <div class="submit__cover">
                    <button class="back">QUAY LẠI</button>
                    <button class="next" id="button-next">Tiếp tục</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        activeMenu('/index');
        activeStep(2);

        var dataLocal = getLocalStorage(STEP2_KEY);
        if (dataLocal != undefined && dataLocal != '') {
            populateForm('#form-step-2', dataLocal);

            // show hidden input
            if ($('input:radio[name="check-dien-tieu-thu"]:checked') != undefined) {
                if ($('input:radio[name="check-dien-tieu-thu"]:checked').val() === 'true') {
                    $('[data-toggle="3a"]').show();
                    $('[data-toggle="3b"]').hide();
                } else {
                    $('[data-toggle="3a"]').hide();
                    $('[data-toggle="3b"]').show();
                }
            }

            if ($('input:radio[name="check-loai-hoat-dong"]:checked') != undefined) {
                if ($('input:radio[name="check-loai-hoat-dong"]:checked').val() === 'true') {
                    $('[data-toggle="1a"]').show();
                } else {
                    $('[data-toggle="1a"]').hide();
                }
            }
        }

        $('input:radio[data-toggle-value="3a"]').change(function() {
            $('[data-toggle="3a"]').slideDown(300);
            $('[data-toggle="3b"]').slideUp(300);
        });

        $('input:radio[data-toggle-value="3b"]').change(function() {
            $('[data-toggle="3a"]').slideUp(300);
            $('[data-toggle="3b"]').slideDown(300);
            var input = $('input[name="dien-tieu-thu-uoc-tinh"]');
            var dienTieuThuUocTinh = $('input[name="tong-dien-nang-tieu-thu"]').val().replace(/,/g,'') * (0.03);
            input.val(numberWithCommas(dienTieuThuUocTinh.round(2)));
        });

        $('input[name="tong-dien-nang-tieu-thu"]').on('input', () => {
            var check = $('input[type=radio][name=check-dien-tieu-thu]:checked');
            if (check.length != 0 && (check.val()).toLowerCase() === 'false') {
                var input = $('input[name="dien-tieu-thu-uoc-tinh"]');
                var dienTieuThuUocTinh = $('input[name="tong-dien-nang-tieu-thu"]').val().replace(/,/g,'') * (0.03);
                input.val(numberWithCommas(dienTieuThuUocTinh.round(2)));
            }
        });

        var form = $('#form-step-2');
        $('#button-next').click((e) => {
            e.preventDefault();
            var check = 0;
            var radioLoaiHoatDong = $('input[type=radio][name=check-loai-hoat-dong]:checked');
            if (radioLoaiHoatDong.length == 0) {
                $('input[type=radio][name=check-loai-hoat-dong]').parents(".form-group.btn-radio").addClass('error');
                check++;
            } else {
                if ((radioLoaiHoatDong.val()).toLowerCase() === 'true') {
                    var input = $('input[name="loai-hoat-dong"]');
                    if (input.val() == '') {
                        input.parents(".form-group").addClass('error');
                        check++;
                    }
                }
            }

            var dienNangTieuThu = 0;
            var radioDienTieuThu = $('input[type=radio][name=check-dien-tieu-thu]:checked');
            if (radioDienTieuThu.length == 0) {
                $('input[type=radio][name=check-dien-tieu-thu]').parents(".form-group.btn-radio").addClass('error');
                check++;
            } else {
                var valRadio = (radioDienTieuThu.val()).toLowerCase();
                if (valRadio === 'true') {
                    var input = $('input[name="dien-tieu-thu"]');
                    if (input.val() == '') {
                        input.parents(".form-group").addClass('error');
                        check++;
                    }
                    dienNangTieuThu = input.val();
                } else if (valRadio === 'false') {
                    var input = $('input[name="dien-tieu-thu-uoc-tinh"]');
                    if (input.val() == '') {
                        var dienTieuThuUocTinh = $('input[name="tong-dien-nang-tieu-thu"]').val().replace(/,/g,'') * (0.03);
                        input.val(dienTieuThuUocTinh.round(2));
                    }
                }
            }

            if (check > 0 || !checkNull('#form-step-2'))
                return false;

            var tongDienTieuThu = $('input[name="tong-dien-nang-tieu-thu"]').val().replace(/,/g,'');
            if (dienNangTieuThu >= tongDienTieuThu) {
                $('input[name="dien-tieu-thu"]').parents('.form-group').addClass('error');
                $('input[name="dien-tieu-thu"]').nextAll('.hint').text('Điện tiêu thụ cho khu vực ngoài phạm vi đánh giá (nhà ăn, văn phòng, …) phải nhỏ hơn tổng điện năng tiêu thụ của cơ sở');
                return false;
            }
            setLocalStorage(STEP2_KEY, JSON.stringify($("#form-step-2").serializeObject()));
            window.location.href = './step-3' + window.location.search;
        });

        $('button.back').click((e) => {
            e.preventDefault();
            window.location.href = './' + window.location.search;
        });
    });
</script>

<?php
include_once('./views/footer.php');
?>
<?php
$title = "Step 3";
include_once('./views/header.php');
?>

<div class="main__content">
    <?php include_once("./views/top.php"); ?>

    <div class="content mt-5">
        <h3 class="main__tit">Số Liệu Sản Lượng Sản Xuất Theo Loại Sản Phẩm</h3>
        <div class="form__content form__add__style pt-5">
            <form id="form-step-3">
                <!--  start ca tron-->
                <div class="form-tit d-flex justify-content-between align-items-center">
                    <p class="tit left">Loại sản phẩm cá da trơn</p>
                    <p class="tit right">Sản lượng cá (kg)</p>
                </div>
                <!-- start select-->
                <div class="form-group add__another__input san-luong-ca-1">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-1">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-1" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-2" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-2">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-2" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-3" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-3">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-3" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-4" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-4">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-4" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-5" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-5">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-5" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-6" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-6">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-6" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-7" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-7">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-7" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-8" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-8">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-8" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-9" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-9">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-9" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>

                <div class="form-group add__another__input san-luong-ca-10" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-ca-da-tron-10">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-10" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <!-- end select-->
                <div class="form-group add__another__input san-luong-ca-11">
                    <div class="input__cover">
                        <label>Sản phẩm khác<span class="hint">Không được để trống</span></label>
                        <input type="text" class="input__style" placeholder="Nhập tên sản phẩm khác" name="loai-san-pham-ca-da-tron-11">
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-11" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-12" style="display: none;">
                    <div class="input__cover">
                        <label>Sản phẩm khác<span class="hint">Không được để trống</span></label>
                        <input type="text" class="input__style" placeholder="Nhập tên sản phẩm khác" name="loai-san-pham-ca-da-tron-12">
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-12" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-ca-13" style="display: none;">
                    <div class="input__cover">
                        <label>Sản phẩm khác<span class="hint">Không được để trống</span></label>
                        <input type="text" class="input__style" placeholder="Nhập tên sản phẩm khác" name="loai-san-pham-ca-da-tron-13">
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-ca-13" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <!--  end ca tron-->

                <!--  start tom-->
                <div class="form-tit pt-5 d-flex justify-content-between align-items-center">
                    <p class="tit left">Loại sản phẩm tôm</p>
                    <p class="tit right">Sản lượng tôm (kg)</p>
                </div>
                <!-- start select-->
                <div class="form-group add__another__input san-luong-tom-1">
                    <div class="select__cover">
                        <select name="loai-san-pham-tom-1">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-tom-1" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-tom-2" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-tom-2">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-tom-2" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-tom-3" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-tom-3">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-tom-3" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-tom-4" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-tom-4">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-tom-4" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-tom-5" style="display: none;">
                    <div class="select__cover">
                        <select name="loai-san-pham-tom-5">
                        </select>
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-tom-5" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>

                <!-- end select-->
                <div class="form-group add__another__input san-luong-tom-6">
                    <div class="input__cover">
                        <label>Sản phẩm khác<span class="hint">Không được để trống</span></label>
                        <input type="text" class="input__style" placeholder="Nhập tên sản phẩm khác" name="loai-san-pham-tom-6">
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-tom-6" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-tom-7" style="display: none;">
                    <div class="input__cover">
                        <label>Sản phẩm khác<span class="hint">Không được để trống</span></label>
                        <input type="text" class="input__style" placeholder="Nhập tên sản phẩm khác" name="loai-san-pham-tom-7">
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-tom-7" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <div class="form-group add__another__input san-luong-tom-8" style="display: none;">
                    <div class="input__cover">
                        <label>Sản phẩm khác<span class="hint">Không được để trống</span></label>
                        <input type="text" class="input__style" placeholder="Nhập tên sản phẩm khác" name="loai-san-pham-tom-8">
                    </div>
                    <input type="text" class="input__style" placeholder="Nhập số ở đây" name="san-luong-tom-8" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');">
                </div>
                <!--  end tom-->



                <!--btn-->
                <div class="submit__cover">
                    <button class="back">QUAY LẠI</button>
                    <button class="next" id="button-next">Tiếp tục</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        activeMenu('/index');
        activeStep(3);

        var caDaTron = $('[name^="loai-san-pham-ca-da-tron"]').length;
        var selectCaDaTron = $('select[name^="loai-san-pham-ca-da-tron"]').length + 1;
        for (let i = 1; i <= caDaTron; i++) {
            if (i < selectCaDaTron) {
                $('select[name="loai-san-pham-ca-da-tron-' + i + '"]').change(() => {
                    var value = $('input[name="san-luong-ca-' + i + '"]').val();
                    if (value != undefined && value != '') {
                        $('.san-luong-ca-' + (i + 1) + '').show();
                    }
                });
            } else {
                $('input[name="loai-san-pham-ca-da-tron-' + i + '"]').on('input', () => {
                    var value = $('input[name="san-luong-ca-' + i + '"]').val();
                    if (value != undefined && value != '') {
                        $('.san-luong-ca-' + (i + 1) + '').show();
                    }
                });
            }

            $('input[name="san-luong-ca-' + i + '"]').on('input', () => {
                var value = $('[name="loai-san-pham-ca-da-tron-' + i + '"]').val();
                if (value != undefined && value != '' && value != '0') {
                    $('.san-luong-ca-' + (i + 1) + '').show();
                }
            });
        }

        var loaiSPTom = $('[name^="loai-san-pham-tom"]').length;
        var selectTom = $('select[name^="loai-san-pham-tom"]').length + 1;
        for (let i = 1; i <= loaiSPTom; i++) {
            if (i < selectTom) {
                $('select[name="loai-san-pham-tom-' + i + '"]').change(() => {
                    var value = $('input[name="san-luong-tom-' + i + '"]').val();
                    if (value != undefined && value != '') {
                        $('.san-luong-tom-' + (i + 1) + '').show();
                    }
                });
            } else {
                $('input[name="loai-san-pham-tom-' + i + '"]').on('input', () => {
                    var value = $('input[name="san-luong-tom-' + i + '"]').val();
                    if (value != undefined && value != '') {
                        $('.san-luong-tom-' + (i + 1) + '').show();
                    }
                });
            }

            $('input[name="san-luong-tom-' + i + '"]').on('input', () => {
                var value = $('[name="loai-san-pham-tom-' + i + '"]').val();
                if (value != undefined && value != '0') {
                    $('.san-luong-tom-' + (i + 1) + '').show();
                }
            });
        }

        getProductType(PRODUCT_TYPE_FISH, [
            'select[name="loai-san-pham-ca-da-tron-1"]',
            'select[name="loai-san-pham-ca-da-tron-2"]',
            'select[name="loai-san-pham-ca-da-tron-3"]',
            'select[name="loai-san-pham-ca-da-tron-4"]',
            'select[name="loai-san-pham-ca-da-tron-5"]',
            'select[name="loai-san-pham-ca-da-tron-6"]',
            'select[name="loai-san-pham-ca-da-tron-7"]',
            'select[name="loai-san-pham-ca-da-tron-8"]',
            'select[name="loai-san-pham-ca-da-tron-9"]',
            'select[name="loai-san-pham-ca-da-tron-10"]'
        ], true);
        getProductType(PRODUCT_TYPE_SHRIMP, [
            'select[name="loai-san-pham-tom-1"]',
            'select[name="loai-san-pham-tom-2"]',
            'select[name="loai-san-pham-tom-3"]',
            'select[name="loai-san-pham-tom-4"]',
            'select[name="loai-san-pham-tom-5"]'
        ], true);

        var form = $('#form-step-3');
        $('#button-next').click((e) => {
            var check = 0;
            e.preventDefault();
            if (!checkNull('#form-step-3'))
                return false;
            setLocalStorage(STEP3_KEY, JSON.stringify($("#form-step-3").serializeObject()));
            window.location.href = './step-4' + window.location.search;
        });

        $('button.back').click((e) => {
            e.preventDefault();
            window.location.href = './step-2' + window.location.search;
        });
    });
</script>

<?php
include_once('./views/footer.php');
?>
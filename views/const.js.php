<?php

?>
<script>
    const URL_API = "http://112.197.2.168:1337";
    const PRODUCT_TYPE_FISH = "fish";
    const PRODUCT_TYPE_SHRIMP = "shrimp";
    const STEP1_KEY = "step1";
    const STEP2_KEY = "step2";
    const STEP3_KEY = "step3";
    const ROLE_ADMIN = "Admin";
    const ROLE_USER = "Registered";
    const PERMISSION_ADMIN = 'manageUser, manageForm, referenceGraph';
    const PERMISSION_USER = 'manageForm';
</script>
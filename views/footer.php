<footer>
    <div class="ft__cover container">
        <h3 class="ft__tit font-weight-bold mb-4">
            TRUNG TÂM NGHIÊN CỨU VÀ PHÁT TRIỂN VỀ TIẾT KIỆM NĂNG LƯỢNG
        </h3>
        <div class="ft__content">
            <p class="ft__line">
                <span class="icon"><img src="images/ic-address.svg" alt="icon"></span><span class="tit">Địa chỉ:</span><span class="txt"><a href="#" target="_blank">224 Điện Biên Phủ, Phường 7, Quận 3, Tp.HCM</a> </span>
            </p>
            <p class="ft__line"><span class="icon"><img src="images/ic-phone.svg" alt="icon"></span><span class="tit">Tel:</span><span class="txt"><a href="tel:842839302393">84.28.3.9302393/ 39307350</a> </span></p>

            <p class="ft__line"><span class="icon"><img src="images/ic-fax.svg" alt="icon"></span><span class="tit">Fax:</span><span class="txt"><a href="#"> 84.28 3.9307350</a> </span></p>
            <p class="ft__line"><span class="icon"><img src="images/ic-email.svg" alt="icon"></span><span class="tit">Email:</span><span class="txt"><a href="mailto:enerteam@enerteam.org">enerteam@enerteam.org</a> </span></p>
            <p class="ft__line"><span class="icon"><img src="images/ic-web.svg" alt="icon"></span><span class="tit">Website:</span><span class="txt"><a href="www.enerteam.org" target="_blank">www.enerteam.org</a> </span></p>
        </div>
    </div>

</footer>
<!-- Modal tao tai khoan-->
<div class="modal modal__dangnhap" id="taikhoan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="images/ic-cloose.svg" alt="icon">
            </button>
            <div class="modal-body d-flex align-items-start justify-content-start">
                <div class="left">
                    <h4 class="tit tit-dangnhap pb-1">Đăng nhập</h4>
                    <h4 class="tit tit-dangki pb-1">Đăng kí</h4>
                    <div class="txt pb-4">Đăng nhập ngay với Enerteam để lưu lại các kết quả, chỉnh sửa thông tin bài báo cáo </div>
                    <img src="images/img-dangnhap.svg" alt="img">
                </div>
                <div class="right">
                    <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="dangnhap-tab" data-toggle="tab" href="#dangnhap" role="tab" aria-controls="home" aria-selected="true">Đăng nhập</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="dangki-tab" data-toggle="tab" href="#dangki" role="tab" aria-controls="dangki" aria-selected="false">Tạo tài khoản</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade" id="dangnhap" role="tabpanel" aria-labelledby="dangnhap-tab">
                            <form id="form-dang-nhap">
                                <div class="form-group">
                                    <label>Email hoặc tên đăng nhập<span class="hint">Tên đăng nhập hoặc mật khẩu không đúng</span></label>
                                    <input type="text" class="input__style" placeholder="Nhập email hoặc tên đăng nhập" name="identifier" data-required>
                                </div>
                                <div class="form-group">
                                    <label>Mật khẩu<span class="hint">Tên đăng nhập hoặc mật khẩu không đúng</span></label>
                                    <input type="password" class="input__style" placeholder="Nhập mật khẩu" name="password" data-required>
                                </div>
                                <p class="dangnhap__note">Quên mật khẩu. Nhấn vào <a class="link-quen-mat-khau" href="#">đây</a></p>
                                <div class="submit__cover dangnhap">
                                    <button class="next" id="button-dang-nhap">Đăng nhập</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="dangki" role="tabpanel" aria-labelledby="dangki-tab">
                            <form id="form-dang-ky" autocomplete="off">
                                <div class="form-group">
                                    <label>Họ tên <span class="hint">Tên đăng nhập hoặc mật khẩu không đúng</span></label>
                                    <input type="text" class="input__style" placeholder="Nhập họ tên" autocomplete="off" name="username" data-required>
                                </div>
                                <div class="form-group">
                                    <label>Số điện thoại<span class="hint">Số điện thoại không đúng</span></label>
                                    <input type="text" class="input__style" placeholder="Số điện thoại" autocomplete="off" name="phone" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                                </div>
                                <div class="form-group">
                                    <label>Email <span class="hint">Tên đăng nhập hoặc mật khẩu không đúng</span></label>
                                    <input type="email" class="input__style" placeholder="Nhập email" autocomplete="off" name="email" data-required>
                                </div>
                                <div class="form-group">
                                    <label>Mật khẩu<span class="hint">Mật khẩu không đúng</span></label>
                                    <input type="password" class="input__style" placeholder="Nhập mật khẩu" autocomplete="off" name="password" data-required>
                                </div>

                                <p class="dangnhap__note">Khi bạn nhấn tạo tài khoản, bạn đã đồng ý với<a href="#"> điều
                                        kiện sự dụng và chính sách của Enerteam</a></p>
                                <div class="submit__cover dangnhap">
                                    <button class="next" id="button-dang-ky">Tạo tài khoản</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end modal tao tai khoan-->
<!--  start modal quen matkhau-->
<div class="modal modal__quenmatkhau" id="modal-quenmatkhau" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="images/ic-cloose.svg" alt="icon">
            </button>
            <div class="modal-body text-center">
                <div class="content">
                    <h4 class="tit pb-1">Quên mật khẩu</h4>
                    <div class="txt pb-3">Vui lòng cung cấp email để lấy lại mật khẩu</div>
                    <form id="form-quen-mat-khau" autocomplete="off">
                        <div class="form-group">
                            <input type="text" class="input__style" placeholder="Nhập email" name="email">
                        </div>
                        <div class="submit__cover dangnhap">
                            <button class="next" id="button-quen-mat-khau">Gửi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<?php include_once('const.js.php') ?>
<?php include_once('utils.js.php') ?>
<script src="js/script.js"></script>
<style>
    .ui-datepicker-unselectable.ui-state-disabled {
        color: #e5e5e5;
    }
</style>
<script>
    $(document).ready(() => {
        loginBackground();

        $('a.link-quen-mat-khau').click((e) => {
            e.preventDefault();
            $('#taikhoan').modal('hide');
            $('#modal-quenmatkhau').modal('show');
        });

        $('#button-dang-nhap').click((e) => {
            e.preventDefault();
            // get form data
            var form = $('#form-dang-nhap');
            var form_data = JSON.stringify(form.serializeObject());

            $.ajax({
                url: URL_API + "/auth/local",
                type: "POST",
                contentType: 'application/json',
                data: form_data,
                success: function(result) {
                    // store jwt to cookie
                    setCookie("jwt", result.jwt, 2);
                    setCookie("user", result.user.username, 2);
                    setCookie("result", result.user.id, 2);
                    setLocalStorage("login_data", form_data);

                    if (result.user.role.name === ROLE_ADMIN && result.user.role.type === ROLE_ADMIN.toLowerCase()) {
                        setCookie("permissions", PERMISSION_ADMIN, 2);
                    } else {
                        setCookie("permissions", PERMISSION_USER, 2);
                    }
                    
                    // redirect to homepage
                    swal({
                        title: "Thông báo",
                        text: "Đăng nhập thành công",
                        icon: "success",
                        timer: 3000
                    }).then((value) => {
                        $('#taikhoan').modal('hide');
                        $('#modal-quenmatkhau').modal('hide');
                        window.location.href = "/admin";
                    });
                },
                error: function(xhr, textStatus) {
                    swal({
                        title: "Thông báo",
                        text: "Tên đăng nhập hoặc mật khẩu không đúng",
                        icon: "error"
                    });
                }
            });

            return false;
        });

        $('#button-dang-ky').click((e) => {
            e.preventDefault();
            // get form data
            var form = $('#form-dang-ky');
            var form_data = JSON.stringify(form.serializeObject());

            $.ajax({
                url: URL_API + "/auth/local/register",
                type: "POST",
                contentType: 'application/json',
                data: form_data,
                success: function(result) {
                    if (result.user != undefined && result.user != '') {
                        // redirect to homepage
                        swal({
                            title: "Thông báo",
                            html: '<p>Đăng ký thành công.</p><br><p> Vui lòng kiểm tra email hoặc thư mục Thư rác (Spam) để kích hoạt tài khoản.</p>',
                            icon: "success"
                        }).then((value) => {
                            $('#taikhoan').modal('hide');
                            $('#modal-quenmatkhau').modal('hide');
                        });
                    }
                },
                error: function(xhr, textStatus) {
                    swal({
                        title: "Thông báo",
                        text: "Email đã có trong hệ thống",
                        icon: "error"
                    });
                    console.log(xhr);
                }
            });

            return false;
        });

        $('#button-quen-mat-khau').click((e) => {
            e.preventDefault();
            // get form data
            var form = $('#form-quen-mat-khau');
            var form_data = JSON.stringify(form.serializeObject());

            $.ajax({
                url: URL_API + "/auth/forgot-password",
                type: "POST",
                contentType: 'application/json',
                data: form_data,
                success: function(result) {
                    if (result.ok != undefined && result.ok != '') {
                        // redirect to homepage
                        swal({
                            title: "Thông báo",
                            text: "Vui lòng kiểm tra email để lấy lại mật khẩu.",
                            icon: "success"
                        }).then((value) => {
                            $('#taikhoan').modal('hide');
                            $('#modal-quenmatkhau').modal('hide');
                        });
                    }
                },
                error: function(xhr, textStatus) {
                    swal({
                        title: "Thông báo",
                        text: "Hệ thống email đang bận, vui lòng thử lại sau.",
                        icon: "error"
                    });
                    console.log(xhr);
                }
            });

            return false;
        });

        $('a.logout').click((e) => {
            e.preventDefault();
            logout();
        });
    });
</script>
</body>

</html>
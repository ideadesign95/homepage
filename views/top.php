<div class="top">
    <h2 class="big__tit container pt-2 pb-2 pt-lg-5 pb-lg-4 pt-sm-3 pb-sm-3">
        <p class="text-center text-uppercase">TÍNH TOÁN ĐỊNH MỨC TIÊU HAO NĂNG LƯỢNG TRONG NGÀNH CÔNG NGHIỆP CHẾ BIẾN THỦY SẢN</p>
        <p class="text-center text-uppercase">ÁP DỤNG CHO QUÁ TRÌNH CHẾ BIẾN CÔNG NGHIỆP CỦA CÁC NHÓM SẢN PHẨM CÁ DA TRƠN VÀ TÔM</p>
        <p class="text-center text-uppercase">THEO THÔNG TƯ 52/2018/TT-BCT</p>
    </h2>
    <div class="steps__cover mb-none">
        <div class="steps__bg">
            <ul class="steps d-flex align-items-center justify-content-between">
                <li>Bước 1</li>
                <li>Bước 2</li>
                <li>Bước 3</li>
                <li>Bước 4</li>
            </ul>
        </div>
        <ul class="name__step d-flex align-items-start justify-content-between">
            <li>Nhập thông tin chung</li>
            <li>Số liệu tiêu thụ điện & thông số sản xuất</li>
            <li>Số liệu sản lượng sản xuất theo loại sản phẩm</li>
            <li>kết quả tính toán & đánh giá</li>
        </ul>
    </div>
    <div class="mb__steps__cover ds-none">
        <div class="mb-steps-cover">
            <div class="mb-step">
                <div class="mb-step-top js_chooseme">Bước 1</div>
                <div class="mb-step-btm js_showarchive">Nhập thông tin chung</div>
            </div>

            <div class="mb-step">
                <div class="mb-step-top js_chooseme">Bước 2</div>
                <div class="mb-step-btm js_showarchive">Số Liệu Tiêu Thụ Điện & Thông Số Sản Xuất</div>
            </div>

            <div class="mb-step">
                <div class="mb-step-top js_chooseme">Bước 3</div>
                <div class="mb-step-btm js_showarchive">Số Liệu Sản Lượng Sản Xuất Theo Loại Sản Phẩm</div>
            </div>

            <div class="mb-step">
                <div class="mb-step-top js_chooseme">Bước 4</div>
                <div class="mb-step-btm js_showarchive">Kết Quả Tính Toán & Đánh Giá</div>
            </div>
        </div>
    </div>

</div>
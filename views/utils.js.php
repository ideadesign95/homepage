<?php

?>
<script>
    Number.prototype.round = function(places) {
        return +(Math.round(this + "e+" + places)  + "e-" + places);
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }
    
    // function to make form values to json format
    $.fn.serializeObject = function() {
        var formData = {};
        var formArray = this.serializeArray();

        for (var i = 0, n = formArray.length; i < n; ++i)
            formData[formArray[i].name] = formArray[i].value.replace(/,(?=\d{3})/g, '');

        return formData;
    };

    function populateForm(formSelector, data) {
        if (!Array.isArray(data))
            data = $.parseJSON(data);

        $.each(data, function(key, value) {
            var $ctrl = $('[name=' + key + ']', formSelector);
            if ($ctrl.is('select')) {
                $("option", $ctrl).each(function() {
                    if (this.value == value) {
                        this.selected = true;
                    }
                    $ctrl.trigger('change');
                });
            } else {
                switch ($ctrl.attr("type")) {
                    case "text":
                    case "hidden":
                    case "textarea":
                    case "email":
                        $ctrl.val(value);
                        $ctrl.trigger('input');
                        break;
                    case "radio":
                    case "checkbox":
                        $ctrl.each(function() {
                            if ($(this).attr('value') == value) {
                                $(this).attr("checked", value);
                            }
                        });
                        break;
                }
            }
        });
    };

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }

            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    function setLocalStorage(key, value) {
        localStorage.setItem(key, value);
    }

    function getLocalStorage(key) {
        return localStorage.getItem(key);
    }

    function removeLocalStorage(key) {
        localStorage.removeItem(key);
    }

    function clearLocalStorage() {
        localStorage.clear();
    }

    function nextStep(key, formSelector, url) {
        var formData = $(formSelector).serializeObject();
        setLocalStorage(key, formData);
        window.location.href(url);
    }

    function backStep(key, formSelector, url) {
        var formData = $(formSelector).serializeObject();
        setLocalStorage(key, formData);
        window.location.href(url);
    }

    function activeMenu(url) {
        $('a.nav-link[href="' + url + '"]').parent().addClass('active');
    };

    function activeStep(step) {
        for (let i = 1; i <= step; i++) {
            $('div.top > div.steps__cover  ul li:nth-child(' + i + ')').addClass('active');
            $('.mb__steps__cover > .mb-steps-cover > .mb-step:nth-child(' + i + ')').addClass('active');
        }
    }

    $.fn.hasAttr = function(name) {
        return this.attr(name) !== undefined;
    };

    function checkNull(formSelector) {
        var check = 0;
        $(formSelector + ' *').find(':input').each(function() {
            var input = $(this);
            if (input.hasAttr('data-required') && input.val() == '') {
                if (input.attr('name').includes("-ngay"))
                    input.addClass('error');
                else {
                    input.parents(".form-group").addClass('error');
                    input.next('.hint').text('Vui lòng nhập thông tin bắt buộc');
                }

                check++;
            } else {
                input.removeClass('error');
                input.parents(".form-group").removeClass('error');
                input.next('.hint').text('');
            }
        });

        if (check > 0)
            return false;

        return true;
    }

    function getProductType(type = "fish", selectors = [], isThirdStep = false) {
        $.ajax({
            // headers: {
            //     'Authorization': 'Bearer ' + getCookie('jwt')
            // },
            url: URL_API + "/product-types?type=" + type,
            type: "GET",
            contentType: 'application/json',
            success: function(result) {
                selectors.forEach(selector => {
                    $(selector).empty();
                    $(selector).append('<option value="0">Chọn loại sản phẩm từ danh sách</option>');
                    if (result != null && result.length > 0) {
                        $.each(result, function(index, value) {
                            if (value != null && value != '')
                                $(selector).append('<option data-id="' + value.id + '" value="' + value.value + '">' + value.label + '</option>');
                        });
                    }
                });

                if (isThirdStep == true) {
                    var dataLocal = getLocalStorage(STEP3_KEY);
                    if (dataLocal != undefined && dataLocal != '') {
                        populateForm('#form-step-3', dataLocal);
                    }
                }
            },
            error: function(jqXHR, error, errorThrown) {
                alert("Có lỗi xảy ra!");
                console.log(jqXHR);
            },
        });
    }

    function isObject(obj) {
        return (typeof obj === "object" && !Array.isArray(obj) && obj !== null);
    }

    function isTodayGreater(d, m, y) {
        var todayDate = new Date(); //Today Date    
        var dateOne = new Date(y, m, d);
        return todayDate > dateOne;
    }

    function toFixedNumber(x) {
        if (Math.abs(x) < 1.0) {
            var e = parseInt(x.toString().split('e-')[1]);
            if (e) {
                x *= Math.pow(10, e - 1);
                x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
            }
        } else {
            var e = parseInt(x.toString().split('+')[1]);
            if (e > 20) {
                e -= 20;
                x /= Math.pow(10, e);
                x += (new Array(e + 1)).join('0');
            }
        }
        return x;
    }

    function getJson(key) {
        var data = getLocalStorage(key);
        return JSON.parse(data);
    }

    function getParameterByName(name, url = window.location.href) {
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function logout() {
        document.cookie = "jwt=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        document.cookie = "user=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        document.cookie = "permissions=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        localStorage.removeItem('login_data');
        window.location.reload();
    }

    function loginBackground() {
        var cookie = getCookie('jwt');
        var user = getCookie('user');
        var uid = getCookie('result');
        var form_data = getLocalStorage('login_data');
        if ((cookie == '' || user == '' || uid == '') && (form_data != undefined && form_data != '')) {
            $.ajax({
                url: URL_API + "/auth/local",
                type: "POST",
                contentType: 'application/json',
                data: form_data,
                success: function(result) {
                    // store jwt to cookie
                    setCookie("jwt", result.jwt, 2);
                    setCookie("user", result.user.username, 2);
                    setCookie("result", result.user.id, 2);
                    setLocalStorage("login_data", form_data);
                    $('#taikhoan').modal('hide');
                    $('#modal-quenmatkhau').modal('hide');
                    window.location.reload();
                    return true;
                },
                error: function(xhr, textStatus) {
                    localStorage.removeItem('login_data');
                    document.cookie = "jwt=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                    document.cookie = "user=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                    console.log(xhr);
                }
            });
        }
        return false;
    }
</script>
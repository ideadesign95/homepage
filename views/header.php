<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
?>

<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=no">
    <title><?php echo empty($title) ? "Enerteam" : "${title} - Enerteam"; ?></title>
    <link rel="shortcut icon" href="http://enerteam.org/wp-content/themes/fokatech/favicon.png">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font-face.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-3.2.1.min.js"></script>
</head>

<style>
    .login.dropdown-toggle::after {
        margin-left: 15px;
    }
    .dropdown-item.active, .dropdown-item:active {
        background-color: #00B849;
    }
</style>

<body>
    <header>
        <div class="header__top d-flex justify-content-between align-items-center align-content-center">
            <img class="left" src="images/loogo-left.svg" alt="logo">
            <img class="right" src="images/main-logo.svg" alt="logo">
        </div>
        <div class="nar__cover">
            <nav class="navbar navbar-expand-lg nav__custom ">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"><img src="images/ic-menu.svg" alt="icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="/index">Trang chủ<span class="active"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/giai-phap">Giải pháp</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/do-thi-tham-khao">Đồ thị tham khảo</a>
                        </li>
                    </ul>
                </div>
                <div class="login__cover">
                    <?php if (empty($_COOKIE['jwt']) || empty($_COOKIE['user']) || empty($_COOKIE['result'])) { ?>
                        <button class="btn__account login" data-toggle="modal" data-target="#taikhoan">Đăng ký</button>
                        <button class="btn__account signup" data-toggle="modal" data-target="#taikhoan">Đăng nhập</button>
                    <?php } else { ?>
                        <div class="dropdown">
                            <button type="button" class="btn dropdown-toggle btn__account login" data-toggle="dropdown"><?php echo $_COOKIE['user']; ?></button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item dashboard" href="/admin">Quản lý tài khoản</a>
                                <a class="dropdown-item logout" href="#">Đăng xuất</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </nav>
        </div>
    </header>
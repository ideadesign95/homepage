<?php
$title = "Đồ thị tham khảo";
include_once('./views/header.php');
?>

<div class="main__content">
    <div class="content mt-5">
        <h3 class="main__tit">Đồ Thị Tham Khảo</h3>
        <div class="pt-5 pb-5 dothithamkhao__content">

        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        activeMenu('/do-thi-tham-khao');
        getGraphData();
    });

    function getGraphData() {
        $.ajax({
            // headers: {
            //     'Authorization': 'Bearer ' + getCookie('jwt')
            // },
            url: URL_API + "/reference-graphs",
            type: "GET",
            contentType: 'application/json',
            success: function(response) {
                if (response.length > 0) {
                    $.each(response, (key, value) => {
                        var chartData = explodeArray(value.data);
                        drawChart(key, value, chartData);
                    });
                }
            },
            error: function(jqXHR, error, errorThrown) {
                alert("Có lỗi xảy ra!");
                console.log(jqXHR);
            },
        });
    }

    function explodeArray(arr) {
        var tmp = [];
        var labels = [];
        var values = [];
        $.each(arr, (key, value) => {
            labels.push(value.label);
            values.push(value.value);
        });
        tmp['labels'] = labels;
        tmp['values'] = values;
        return tmp;
    }

    function drawChart(key, value, data) {
        $('.dothithamkhao__content').append(appendHtml(key, value.title));

        var formatter = new Intl.NumberFormat("en-US");
        Chart.pluginService.register({
            afterDatasetsDraw: function(chartInstance) {
                var ctx = chartInstance.chart.ctx;

                ctx.font = Chart.helpers.fontString(14, 'bold', Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';
                ctx.fillStyle = '#001408';

                chartInstance.config.data.datasets.forEach(function(dataset) {

                    for (var i = 0; i < dataset.data.length; i++) {
                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                            total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                            mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius) / 2,
                            start_angle = model.startAngle,
                            end_angle = model.endAngle,
                            mid_angle = start_angle + (end_angle - start_angle) / 2;

                        var x = mid_radius * 1.5 * Math.cos(mid_angle);
                        var y = mid_radius * 1.3 * Math.sin(mid_angle);

                        ctx.fillStyle = '#001408';
                        if (i === 0 || i === 3 || i === 7) { // Darker text color for lighter background
                            ctx.fillStyle = '#001408';
                        }
                        var percent = String(Math.round(dataset.data[i] / total * 100)) + "%";
                        // this prints the data number
                        // this prints the percentage
                        ctx.fillText(percent, model.x + x, model.y + y);
                    }
                });
            }
        });

        var data = {
            labels: data['labels'],
            datasets: [{
                data: data['values'],
                backgroundColor: [
                    "#4D72BE",
                    "#DF8244",
                    "#A5A5A5",
                    "#F6C242",
                    "#699AD0",
                    "#7EAB55",
                    "#2C4474",
                    "#944D20",
                    "#636363"
                ]
            }]
        };

        var optionsPie = {
            responsive: false,
            scaleBeginAtZero: true,
            legend: {
                display: false
            },
            tooltips: {
                titleFontSize: 16,
                bodyFontSize: 15,
                callbacks: {
                    label: function(tooltipItem, data) {
                        return data.labels[tooltipItem.index] + ": " +
                            formatter.format(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                    }
                }
            }
        };

        var ctx = $("#chart-" + key).get(0).getContext("2d");

        var top10PieChart = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: optionsPie,

            animation: {
                duration: 0,
                easing: "easeOutQuart",
                onComplete: function() {
                    var ctx = this.chart.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function(dataset) {

                        for (var i = 0; i < dataset.data.length; i++) {
                            var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius) / 2,
                                start_angle = model.startAngle,
                                end_angle = model.endAngle,
                                mid_angle = start_angle + (end_angle - start_angle) / 2;

                            var x = mid_radius * Math.cos(mid_angle);
                            var y = mid_radius * Math.sin(mid_angle);

                            ctx.fillStyle = '#001408';
                            if (i === 3) { // Darker text color for lighter background
                                ctx.fillStyle = '#001408';
                            }
                            var percent = String(Math.round(dataset.data[i] / total * 100)) + "%";
                            // this prints the data number
                            ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                            // this prints the percentage
                            ctx.fillText(percent, model.x + x, model.y + y + 15);
                        }
                    });
                }
            }
        });

        $("#chart-" + key + "-legend").html(top10PieChart.generateLegend());
    }

    function appendHtml(index, title) {
        var html = '';

        html += '<div class="dothi pt-lg-5 pr-lg-5 pb-lg-5 pl-lg-5 p-t-4 pt-5 pr-2 pb-4 pl-2">';
        html += '    <h4 class="tit text-center">' + title + '</h4>';
        html += '    <div class="dothithamkhao__chart">';
        html += '        <div class="piechartlegendleft">';
        html += '            <div id="container">';
        html += '                <canvas id="chart-' + index + '"></canvas>';
        html += '            </div>';
        html += '            <div id="chart-' + index + '-legend" class="pieLegend"></div>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';

        return html;
    }
</script>

<?php
include_once('./views/footer.php');
?>
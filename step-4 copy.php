<?php
$title = "Step 4";
include_once('./views/header.php');
?>

<div class="main__content">
    <?php include_once("./views/top.php"); ?>

    <div class="content mt-5">
        <h3 class="main__tit">Kết Quả Tính Toán & Đánh giá</h3>
        <div class="form__content pt-5 form__ketquaaddstyle">
            <div class="ketqua__cover">
                <form id="form-ket-qua">
                    <div class="form-group add__input__kqua d-flex justify-content-center align-items-center">
                        <label>Điện (hd)</label>
                        <input type="text" class="input__style" value="" name="dien-hd" readonly>
                        <span class="sub">kWh</span>
                    </div>

                    <div class="form-group add__input__kqua d-flex justify-content-center align-items-center">
                        <label>Điện (khác)</label>
                        <input type="text" class="input__style" value="" name="dien-khac" readonly>
                        <span class="sub">kWh</span>
                    </div>

                    <div class="form-group add__input__kqua d-flex justify-content-center align-items-center">
                        <label>Điện (gián tiếp)</label>
                        <input type="text" class="input__style" value="" name="dien-gian-tiep" readonly>
                        <span class="sub">kWh</span>
                    </div>

                    <div class="form-group add__input__kqua d-flex justify-content-center align-items-center">
                        <label>Điện (cb)</label>
                        <input type="text" class="input__style" value="" name="dien-cb" readonly>
                        <span class="sub">kWh</span>
                    </div>

                    <div class="form-group add__input__kqua d-flex justify-content-center align-items-center">
                        <label>Tổng sản lượng quy đổi</label>
                        <input type="text" class="input__style" value="" name="tong-san-luong-quy-doi" readonly>
                        <span class="sub">kg</span>
                    </div>

                    <div class="form-group add__input__kqua d-flex justify-content-center align-items-center">
                        <label>Định mức tiêu hao năng lượng</label>
                        <input type="text" class="input__style" value="" name="dinh-muc-tieu-hao-nang-luong" readonly>
                        <span class="sub">kWh/Tấn</span>
                    </div>

                    <div class="form-group add__input__kqua d-flex justify-content-center align-items-center">
                        <label>Suất tiêu hao năng lượng</label>
                        <input type="text" class="input__style" value="" name="suat-tieu-hao-nang-luong" readonly>
                        <span class="sub">kWh/Tấn</span>
                    </div>
                    <div class="form-group add__input__kqua dat d-flex justify-content-start align-items-center">
                        <label>Đánh giá đạt hay không đạt</label>
                        <span class="kqua d8">ĐẠT</span>
                    </div>
                    <p class="d-block note-kqua text-left" style="display: none;"></p>
                </form>
            </div>
        </div>
        <!-- <div class="dothi__coover">
            <h4 class="dothi__tit text-capitalize font-weight-bold text-center pt-5 pb-5">đồ thị so sánh suất tiêu hao của người dùng so với định mức và cơ sở dữ liệu các người dùng khác</h4>
            <div class="dothi_cover-content" style="width: 100%; overflow-x: auto;overflow-y:hidden">
                <canvas id="myChart" width="680" height="230"></canvas>
            </div>
            <h4 class="chart__tit text-center pt-4">
                So sánh SEC
            </h4>
        </div> -->
    </div>

    <!--btn-->
    <div class="submit__cover text-center final__result">
        <button class="back">QUAY LẠI</button>
        <button class="next" name="exportSheet">XUẤT FILE BÁO CÁO (SHEET FORM)</button>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        activeMenu('/index');
        activeStep(4);

        var slqd = {
            'slqdCa': 0,
            'slqdTom': 0
        };
        var step1 = getJson(STEP1_KEY);
        var step2 = getJson(STEP2_KEY);
        var step3 = getJson(STEP3_KEY);

        if (!isObject(step1)) {
            swal({
                title: "Thông báo",
                text: "Vui lòng nhập đầy đủ thông tin ở Bước 1",
                icon: "error"
            }).then((value) => {
                window.location.href = "./index" + window.location.search;
            });
        } else if (!isObject(step2)) {
            swal({
                title: "Thông báo",
                text: "Vui lòng nhập đầy đủ thông tin ở Bước 2",
                icon: "error"
            }).then((value) => {
                window.location.href = "./step-2" + window.location.search;
            });
        } else if (!isObject(step3)) {
            swal({
                title: "Thông báo",
                text: "Vui lòng nhập đầy đủ thông tin ở Bước 3",
                icon: "error"
            }).then((value) => {
                window.location.href = "./step-3" + window.location.search;
            });
        }

        calculate(slqd, step1, step2, step3);
        // drawChart();

        $('button[name="exportSheet"]').click((e) => {
            e.preventDefault();
            var jwt = getCookie('jwt');
            var formId = Number(getParameterByName('form'));
            // if (jwt == undefined || jwt == '') {
            //     swal({
            //         title: "Thông báo",
            //         text: "Vui lòng đăng nhập để sử dụng chức năng này.",
            //         icon: "warning"
            //     }).then((value) => {
            //         // login
            //         $('.modal__dangnhap#taikhoan').modal('show');
            //         $('#dangnhap-tab').tab('show');
            //     });
            //     return false;
            // }

            var jsonData = {
                'step1': step1,
                'step2': step2,
                'step3': step3,
                'step4': $("#form-ket-qua").serializeObject(),
                'slqd': slqd,
                'form': formId,
                'uid': getCookie('result')
            };

            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + jwt
                },
                url: "/api/export",
                type: "POST",
                data: JSON.stringify(jsonData),
                contentType: 'application/json',
                success: function(response) {
                    console.log(response);
                    if (response.result == false && response.status_code == 403) {
                        swal({
                            title: "Thông báo",
                            text: "Vui lòng đăng nhập để sử dụng chức năng này.",
                            icon: "warning"
                        }).then((value) => {
                            // login
                            $('.modal__dangnhap#taikhoan').modal('show');
                            $('#dangnhap-tab').tab('show');
                        });
                        return false;
                    } else if (response.result == false) {
                        swal({
                            title: "Thông báo",
                            text: response.message,
                            icon: "error"
                        });
                        return false;
                    } else if (response.result == true) {
                        // write
                        var isUpdate = formId != 0;
                        writeForm(isUpdate, formId, jsonData);
                        swal({
                            title: "Thông báo",
                            text: response.message,
                            icon: "success"
                        }).then((value) => {
                            removeLocalStorage('step1');
                            removeLocalStorage('step2');
                            removeLocalStorage('step3');
                            window.open(
                                window.location.protocol + '//' + window.location.hostname + response.url,
                                '_blank' // <- This is what makes it open in a new window.
                            );
                        });
                        return false;
                    }
                },
                error: function(jqXHR, error, errorThrown) {
                    swal({
                        title: "Thông báo",
                        text: "Có lỗi xảy ra",
                        icon: "error"
                    });
                    console.log(jqXHR);
                },
            });
        });

        $('button.back').click((e) => {
            e.preventDefault();
            window.location.href = './step-3' + window.location.search;
        });
    });

    function calculate(objSlqd, step1, step2, step3) {
        // D1
        $('input[name="dien-hd"]').val(step2["tong-dien-nang-tieu-thu"]);

        // D2
        var a = step2["check-dien-tieu-thu"] == 'true' ? parseFloat(step2["dien-tieu-thu"]) : parseFloat(step2['dien-tieu-thu-uoc-tinh']);
        var b = parseFloat(step2['khoi-luong-da-ban-ra']) * 70;
        var c = parseFloat(step2['san-luong-cho-thue-kho-lanh']) * parseFloat(step2['thoi-gian-cho-thue-kho-lanh']) * 2.5;
        a = isNaN(a) ? 0 : a;
        b = isNaN(b) ? 0 : b;
        c = isNaN(c) ? 0 : c;
        var sum = a + b + c;
        $('input[name="dien-khac"]').val(isNaN(sum) ? 0 : sum);

        // D3
        a = parseFloat(step2['khoi-luong-da-mua-vao']) * 70;
        b = parseFloat(step2['san-luong-thue-kho-lanh']) * parseFloat(step2['thoi-gian-thue-kho-lanh']) * 2.5;
        a = isNaN(a) ? 0 : a;
        b = isNaN(b) ? 0 : b;
        sum = a + b;
        $('input[name="dien-gian-tiep"]').val(isNaN(sum) ? 0 : sum);

        // D4
        a = parseFloat($('input[name="dien-hd"]').val());
        b = parseFloat($('input[name="dien-khac"]').val());
        c = parseFloat($('input[name="dien-gian-tiep"]').val());
        a = isNaN(a) ? 0 : a;
        b = isNaN(b) ? 0 : b;
        c = isNaN(c) ? 0 : c;
        sum = a + b + c;
        $('input[name="dien-cb"]').val(isNaN(sum) ? 0 : sum);

        // D5
        var slqdCa = 0;
        var slqdTom = 0;
        var temp = 0;
        for (const key in step3) {
            if (step3[key] != '' || step3[key] != 0) {
                if (key.includes("loai-san-pham-ca-da-tron")) {
                    temp = isNaN(step3[key]) ? 1 : parseFloat(step3[key]);
                }
                if (key.includes("san-luong-ca")) {
                    temp = parseFloat(temp) * parseFloat(step3[key]);
                    slqdCa += temp;
                }
                if (key.includes("loai-san-pham-tom")) {
                    temp = isNaN(step3[key]) ? 1 : parseFloat(step3[key]);
                }
                if (key.includes("san-luong-tom")) {
                    temp = parseFloat(temp) * parseFloat(step3[key]);
                    slqdTom += temp;
                }
            }
        }
        objSlqd.slqdCa = slqdCa;
        objSlqd.slqdTom = slqdTom;
        if (slqdCa >= slqdTom) {
            $('input[name="tong-san-luong-quy-doi"]').prev('label').text('Tổng sản lượng quy đổi sang cá da trơn');
            $('input[name="suat-tieu-hao-nang-luong"]').prev('label').text('Suất tiêu hao năng lượng quy đổi sang sản phẩm cá da trơn');
            $('input[name="tong-san-luong-quy-doi"]').val((slqdCa * 1) + (slqdTom * 1.78));
        } else {
            $('input[name="tong-san-luong-quy-doi"]').prev('label').text('Tổng sản lượng quy đổi sang tôm');
            $('input[name="suat-tieu-hao-nang-luong"]').prev('label').text('Suất tiêu hao năng lượng quy đổi sang sản phẩm tôm');
            $('input[name="tong-san-luong-quy-doi"]').val((slqdCa * 0.56) + (slqdTom * 1));
        }

        // D6
        if (!isTodayGreater(1, 1, 2026)) {
            if (slqdCa >= slqdTom) {
                $('input[name="dinh-muc-tieu-hao-nang-luong"]').prev('label').text('Định mức tiêu hao năng lượng cho sản phẩm cá da trơn');
                $('input[name="dinh-muc-tieu-hao-nang-luong"]').val(1050);
            } else {
                $('input[name="dinh-muc-tieu-hao-nang-luong"]').prev('label').text('Định mức tiêu hao năng lượng cho sản phẩm tôm');
                $('input[name="dinh-muc-tieu-hao-nang-luong"]').val(2050);
            }
        } else {
            if (slqdCa >= slqdTom) {
                $('input[name="dinh-muc-tieu-hao-nang-luong"]').prev('label').text('Định mức tiêu hao năng lượng cho sản phẩm cá da trơn');
                $('input[name="dinh-muc-tieu-hao-nang-luong"]').val(900);
            } else {
                $('input[name="dinh-muc-tieu-hao-nang-luong"]').prev('label').text('Định mức tiêu hao năng lượng cho sản phẩm tôm');
                $('input[name="dinh-muc-tieu-hao-nang-luong"]').val(1625);
            }
        }

        // D7
        if (slqdCa >= slqdTom) {
            var d4 = parseFloat($('input[name="dien-cb"]').val());
            var d5 = parseFloat($('input[name="tong-san-luong-quy-doi"]').val());
            $('input[name="suat-tieu-hao-nang-luong"]').prev('label').text('Suất tiêu hao năng lượng quy đổi sang sản phẩm cá da trơn');
            $('input[name="suat-tieu-hao-nang-luong"]').val(toFixedNumber(d4 / d5 / 1000));
        }

        // D8
        var d6 = parseFloat($('input[name="dinh-muc-tieu-hao-nang-luong"]').val());
        var d7 = toFixedNumber($('input[name="suat-tieu-hao-nang-luong"]').val());
        if (d7 < d6) {
            $('.note-kqua').show();
            $('.note-kqua').text('Suất tiêu hao năng lượng của cơ sở phù hợp với quy định định mức tiêu hao năng lượng cho quá trình chế biến công nghiệp của các nhóm sản phẩm cá da trơn và tôm');
            $('.d8').text("ĐẠT");
            $('.d8').removeClass('bg-danger');
        } else {
            $('.note-kqua').show();
            $('.note-kqua').text('Suất tiêu hao năng lượng của cơ sở không đáp ứng định mức tiêu hao năng lượng cho quá trình chế biến công nghiệp của các nhóm sản phẩm cá da trơn và tôm');
            $('.d8').text("KHÔNG ĐẠT");
            $('.d8').addClass('bg-danger');
        }
    }

    function drawChart() {
        var ctx = document.getElementById('myChart');
        var secTrungBinh = 1080;
        // var secNhaMay = toFixedNumber($('input[name="suat-tieu-hao-nang-luong"]').val());
        var secNhaMay = parseFloat($('input[name="suat-tieu-hao-nang-luong"]').val()).toFixed(2);
        var secDinhMuc = parseInt($('input[name="dinh-muc-tieu-hao-nang-luong"]').val());
        var max = Math.max(secTrungBinh, secNhaMay, secDinhMuc) + 100;
        var step = Math.ceil(max / 10 / 5) * 5;

        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: ['SEC trung bình', 'SEC nhà máy', 'SEC định mức'],
                datasets: [{
                    label: 'So sánh SEC',
                    data: [secTrungBinh, secNhaMay, secDinhMuc],
                    backgroundColor: [
                        'rgba(150,217,238,1)',
                        'rgba(44,180,221,1)',
                        'rgba(29,141,176,1)'

                    ],
                }]
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false,
                },
                animation: {
                    "onComplete": function() {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.fillStyle = "rgba(0, 0, 0, 1)";
                        ctx.textBaseline = 'bottom';
                        ctx.fontStyle = 'normal';
                        ctx.fontSize = '18';
                        // Loop through each data in the datasets
                        this.data.datasets.forEach(function(dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function(bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x + 30, bar._model.y + 4);

                            });
                        });
                    }
                },
                scales: {
                    yAxes: [{
                        barPercentage: 0.7,
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            fontColor: "#026D8E",
                            fontSize: 18,
                            fontStyle: "500",

                        },

                    }],
                    xAxes: [{
                        gridLines: {
                            display: true,
                            color: "#E8EBE9",
                            lineWidth: 1,
                            zeroLineColor: '#FFFFFF',

                        },
                        ticks: {
                            fontColor: "#001408",
                            fontSize: 18,
                            padding: 5,
                            fontStyle: "normal",
                            min: 000,
                            max: max,
                            stepSize: step,
                        },
                    }]
                },
                legend: {
                    display: false
                },
            }
        });
    }

    function writeForm(isUpdate = false, formId = 0, jsonData) {
        jwt = getCookie('jwt');
        if (jwt != '') {
            var method = isUpdate ? "PUT" : "POST";
            var url = URL_API + (isUpdate ? `/forms/${formId}` : "/forms");
            var uid = getCookie('result');
            const formData = new FormData();
            const data = {};
            data['user_id'] = uid;
            data['created_by'] = uid;
            data['updated_by'] = uid;
            data['data'] = jsonData;
            formData.append('data', JSON.stringify(data));
            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + jwt
                },
                url: url,
                type: method,
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {},
                error: () => {}
            });
        }
    }
</script>

<?php
include_once('./views/footer.php');
?>